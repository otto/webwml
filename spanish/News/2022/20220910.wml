#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.13</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la decimotercera (y última) actualización de su
distribución «antigua estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tras la publicación de esta versión, los equipos de seguridad y responsable de la publicación de Debian ya no
proporcionarán actualizaciones para Debian 10. Los usuarios que deseen continuar recibiendo soporte de seguridad
deberían actualizar a Debian 11 o consultar <url "https://wiki.debian.org/LTS"> para detalles
sobre el subconjunto de arquitecturas y paquetes que están cubiertos por el proyecto de soporte a largo
plazo («LTS»).</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «antigua estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction adminer "Corrige problema de redirección abierta y problemas de ejecución de scripts entre sitios («cross-site scripting») [CVE-2020-35572 CVE-2021-29625]; elasticsearch: no imprime la respuesta si el código HTTP es distinto de 200 [CVE-2021-21311]; proporciona una versión compilada y ficheros de configuración">
<correction apache2 "Corrige problema de denegación de servicio [CVE-2022-22719], problema de «contrabando» de peticiones HTTP («HTTP request smuggling») [CVE-2022-22720], problema de desbordamiento de entero [CVE-2022-22721], problema de escritura fuera de límites [CVE-2022-23943], problema de «contrabando» de peticiones HTTP [CVE-2022-26377], problemas de lectura fuera de límites [CVE-2022-28614 CVE-2022-28615], problema de denegación de servicio [CVE-2022-29404], problema de lectura fuera de límites [CVE-2022-30556] y posible problema de elusión de autenticación por IP [CVE-2022-31813]">
<correction base-files "Actualizado para la versión 10.13">
<correction clamav "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction commons-daemon "Corrige la detección de la JVM">
<correction composer "Corrige vulnerabilidad de inyección de código [CVE-2022-24828]; actualiza patrón de tokens de GitHub; usa la cabecera Authorization en lugar del parámetro de consulta access_token, considerado obsoleto">
<correction debian-installer "Recompilado contra buster-proposed-updates; incrementa la ABI de Linux a la 4.19.0-21">
<correction debian-installer-netboot-images "Recompilado contra buster-proposed-updates; incrementa la ABI de Linux a la 4.19.0-21">
<correction debian-security-support "Actualiza el estado de seguridad de varios paquetes">
<correction debootstrap "Asegura que se puedan seguir creando chroots con /usr no fusionado para versiones anteriores y para chroots de buildd">
<correction distro-info-data "Añade Ubuntu 22.04 LTS, Jammy Jellyfish, y Ubuntu 22.10, Kinetic Kudu">
<correction dropbear "Corrige posible problema de enumeración de nombres de usuario [CVE-2019-12953]">
<correction eboard "Corrige violación de acceso en la selección de motor">
<correction esorex "Corrige fallos en el juego de pruebas en armhf y ppc64el provocados por un uso incorrecto de libffi">
<correction evemu "Corrige fallo de compilación con versiones del núcleo recientes">
<correction feature-check "Corrige algunas comparaciones de versiones">
<correction flac "Corrige problema de escritura fuera de límites [CVE-2021-0561]">
<correction foxtrotgps "Corrige fallo de compilación con versiones de imagemagick más recientes">
<correction freeradius "Corrige fuga de información de canal lateral por el fallo de 1 de cada 2048 handshakes [CVE-2019-13456], denegación de servicio por acceder a una estructura BN_CTX desde varios hilos [CVE-2019-17185] y caída por asignación de memoria que no tiene seguridad en hilos («non-thread safe memory»)">
<correction freetype "Corrige problema de desbordamiento de memoria [CVE-2022-27404]; corrige caídas [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Corrige problemas de desbordamiento de memoria [CVE-2022-25308 CVE-2022-25309]; corrige caída [CVE-2022-25310]">
<correction ftgl "No intenta convertir de PNG a EPS en el caso de latex, ya que nuestro imagemagick tiene el EPS inhabilitado por razones de seguridad">
<correction gif2apng "Corrige desbordamientos de memoria dinámica («heap») [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction gnucash "Corrige fallo de compilación con tzdata reciente">
<correction gnutls28 "Corrige juego de pruebas al combinarlo con OpenSSL 1.1.1e o posterior">
<correction golang-github-docker-go-connections "Omite las pruebas que usan certificados expirados">
<correction golang-github-pkg-term "Corrige compilación en núcleos 4.19 más recientes">
<correction golang-github-russellhaering-goxmldsig "Corrige problema de desreferencia de puntero NULL [CVE-2020-7711]">
<correction grub-efi-amd64-signed "Nueva versión del proyecto original">
<correction grub-efi-arm64-signed "Nueva versión del proyecto original">
<correction grub-efi-ia32-signed "Nueva versión del proyecto original">
<correction grub2 "Nueva versión del proyecto original">
<correction htmldoc "Corrige bucle infinito [CVE-2022-24191], problemas de desbordamiento de entero [CVE-2022-27114] y problema de desbordamiento de memoria dinámica («heap») [CVE-2022-28085]">
<correction iptables-netflow "Corrige regresión de fallo de compilación vía DKMS provocada por cambios en la versión 4.19.191 del núcleo realizados por el proyecto original Linux">
<correction isync "Corrige problemas de desbordamiento de memoria [CVE-2021-3657]">
<correction kannel "Corrige fallo de compilación inhabilitando la generación de documentación en Postscript">
<correction krb5 "Usa SHA256 como Pkinit CMS Digest">
<correction libapache2-mod-auth-openidc "Mejora la validación del parámetro de URL post-logout en logout [CVE-2019-14857]">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libhttp-cookiejar-perl "Corrige fallo de compilación retrasando la fecha de expiración de una cookie de prueba">
<correction libnet-freedb-perl "Cambia la máquina por omisión de la obsoleta freedb.freedb.org a gnudb.gnudb.org">
<correction libnet-ssleay-perl "Corrige fallos de pruebas con OpenSSL 1.1.1n">
<correction librose-db-object-perl "Corrige fallo de prueba después del 6/6/2020">
<correction libvirt-php "Corrige violación de acceso en libvirt_node_get_cpu_stats">
<correction llvm-toolchain-13 "Nuevo paquete fuente para soportar la compilación de versiones más recientes de firefox-esr y de thunderbird">
<correction minidlna "Valida peticiones HTTP para proteger frente a ataques de revinculación de DNS [CVE-2022-26505]">
<correction mokutil "Nueva versión del proyecto original, para permitir gestión de SBAT">
<correction mutt "Corrige desbordamiento de memoria en uudecode [CVE-2022-1328]">
<correction node-ejs "Sanea opciones y objetos nuevos [CVE-2022-29078]">
<correction node-end-of-stream "Solución provisional a un fallo en una prueba">
<correction node-minimist "Corrige problema de contaminación de prototipo [CVE-2021-44906]">
<correction node-node-forge "Corrige problemas de verificación de firmas [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-require-from-string "Corrige una prueba con nodejs &gt;= 10.16">
<correction nvidia-graphics-drivers "Nueva versión del proyecto original">
<correction nvidia-graphics-drivers-legacy-390xx "Nueva versión del proyecto original; corrige problemas de escritura fuera de límites [CVE-2022-28181 CVE-2022-28185]; correcciones de seguridad [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction octavia "Corrige las comprobaciones de los certificados de clientes [CVE-2019-17134]; detecta de forma correcta que el agente se está ejecutando en Debian; corrige la plantilla que genera el script de comprobación de vrrp; añade dependencias de ejecución adicionales; distribuye configuración adicional directamente en el paquete agente">
<correction orca "Corrige uso con WebKitGTK 2.36">
<correction pacemaker "Actualiza relación entre versiones para corregir actualizaciones desde stretch LTS">
<correction pglogical "Corrige fallo de compilación">
<correction php-guzzlehttp-psr7 "Corrige análisis sintáctico de cabeceras incorrecto [CVE-2022-24775]">
<correction postfix "Nueva versión «estable» del proyecto original; no ignora el valor de default_transport configurado por el usuario; if-up.d: no notifica error si postfix todavía no puede enviar correos; corrige entradas duplicadas de bounce_notice_recipient en la salida de postconf">
<correction postgresql-common "pg_virtualenv: escribe el fichero temporal de contraseñas antes de cambiar el propietario del fichero">
<correction postsrsd "Corrige potencial problema de denegación de servicio cuando Postfix envía ciertos campos largos de datos como, por ejemplo, varias direcciones de correo electrónico concatenadas [CVE-2021-35525]">
<correction procmail "Corrige desreferencia de puntero NULL">
<correction publicsuffix "Actualiza los datos incluidos">
<correction python-keystoneauth1 "Actualiza las pruebas para corregir fallo de compilación">
<correction python-scrapy "No envía datos de autenticación con todas las peticiones [CVE-2021-41125]; no expone cookies de dominios cruzados en los redireccionamientos [CVE-2022-0577]">
<correction python-udatetime "Enlaza correctamente con la biblioteca libm">
<correction qtbase-opensource-src "Corrige setTabOrder para widgets compuestos; añade un límite de expansión de entidades XML [CVE-2015-9541]">
<correction ruby-activeldap "Corrige dependencia con ruby-builder, que faltaba">
<correction ruby-hiredis "Omite algunas pruebas no fiables para corregir fallo de compilación">
<correction ruby-http-parser.rb "Corrige fallo de compilación cuando el http-parser utilizado contiene la corrección para CVE-2019-15605">
<correction ruby-riddle "Permite el uso de <q>LOAD DATA LOCAL INFILE</q>">
<correction sctk "Usa <q>pdftoppm</q> en lugar de <q>convert</q> para convertir de PDF a JPEG ya que el segundo falla con la política de seguridad de ImageMagick modificada">
<correction twisted "Corrige problema de validación incorrecta de URI y de métodos HTTP [CVE-2019-12387], validación incorrecta de certificados en el soporte XMPP [CVE-2019-12855], problemas de denegación de servicio en HTTP/2, problemas de «contrabando» de peticiones HTTP («HTTP request smuggling») [CVE-2020-10108 CVE-2020-10109 CVE-2022-24801], problema de revelación de información al seguir redirecciones entre dominios [CVE-2022-21712] y problema de denegación de servicio durante el handshake de SSH [CVE-2022-21716]">
<correction tzdata "Actualiza datos de zona horaria para Irán, Chile y Palestina; actualiza lista de segundos intercalares">
<correction ublock-origin "Nueva versión «estable» del proyecto original">
<correction unrar-nonfree "Corrige problema de escalado de directorios [CVE-2022-30333]">
<correction wireshark "Corrige problema de ejecución de código remoto [CVE-2021-22191] y problemas de denegación de servicio [CVE-2021-4181 CVE-2021-4184 CVE-2021-4185 CVE-2022-0581 CVE-2022-0582 CVE-2022-0583 CVE-2022-0585 CVE-2022-0586]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «antigua estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2021 4836 openvswitch>
<dsa 2021 4852 openvswitch>
<dsa 2021 4906 chromium>
<dsa 2021 4911 chromium>
<dsa 2021 4917 chromium>
<dsa 2021 4981 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5077 librecad>
<dsa 2022 5080 snapd>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5108 tiff>
<dsa 2022 5109 faad2>
<dsa 2022 5111 zlib>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5126 ffmpeg>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5135 postgresql-11>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5144 condor>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5167 firejail>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5173 linux-latest>
<dsa 2022 5173 linux-signed-amd64>
<dsa 2022 5173 linux-signed-arm64>
<dsa 2022 5173 linux-signed-i386>
<dsa 2022 5173 linux>
<dsa 2022 5174 gnupg2>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5185 mat2>
<dsa 2022 5186 djangorestframework>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction elog "Sin desarrollo activo; problemas de seguridad">
<correction libnet-amazon-perl "Depende de una API eliminada">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «antigua estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «antigua estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Actualizaciones propuestas a la distribución «antigua estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Información sobre la distribución «antigua estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


