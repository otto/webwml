#use wml::debian::translation-check translation="925ae76facb2d1bc28343b8f7956afe7f13c0a12"
<define-tag pagetitle>Clausura de DebConf22 en Prizren y anuncio de fechas para DebConf23</define-tag>

<define-tag release_date>2022-07-24</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Hoy domingo, 24 de julio de 2022, se ha clausurado la conferencia anual de
desarrolladores y contribuidores de Debian.
Con 260 asistentes de 38 países y más de
91 eventos entre charlas, debates, 
reuniones informales (BoF, por sus siglas en inglés: «Birds of a Feather»), talleres y otras actividades,
<a href="https://debconf22.debconf.org">DebConf22</a> ha sido un gran éxito.
</p>

<p>
La conferencia ha estado precedida, del 10 al 16 de julio, por el DebCamp anual,
que se ha centrado en trabajo individual y esprints de equipos para colaboración presencial
relacionada con el desarrollo de Debian.
En concreto, este año ha habido esprints para avanzar en el desarrollo de
Mobian/Debian en móviles, sobre compilaciones reproducibles y sobre Python en Debian; 
también ha habido un BootCamp para principiantes, para que conozcan Debian y tengan
algunas experiencias prácticas sobre su uso y sobre la contribución a la comunidad.
</p>

<p>
La conferencia de desarrolladores de Debian propiamente dicha comenzó el domingo, 17 de julio de 2022.
Junto a actividades como la tradicional charla «"Bits" del líder del proyecto Debian»,
la continua fiesta de firma de claves, charlas relámpago y el anuncio de la DebConf del próximo año
(<a href="https://wiki.debian.org/DebConf/23">DebConf23</a> en Cochín, la India), 
ha habido varias sesiones relacionadas con equipos de lenguajes de programación, como Python, Perl y Ruby,
así como actualizaciones sobre varios proyectos y
equipos internos de Debian, sesiones de debate informales (BoF) de muchos equipos técnicos
(soporte a largo plazo, herramientas para Android, distribuciones derivadas de Debian, instalador de Debian e imágenes,
ciencia...) y de comunidades locales (Debian Brasil, Debian India, los equipos locales de Debian), 
y muchos otros actos de interés acerca de Debian y del software libre.
</p>

<p>
La <a href="https://debconf22.debconf.org/schedule/">programación</a>
se actualizaba a diario con actividades ad-hoc planificadas por asistentes
a la conferencia durante el desarrollo de la misma. Varias actividades que no se pudieron
organizar en años anteriores debido a la pandemia de COVID han vuelto a la programación de la
conferencia: una feria de empleo, noche de micrófonos abiertos y poesía, la tradicional fiesta de queso y vino,
las fotografías de grupo y la excursión.
</p>

<p>
Para quienes no han podido asistir, la mayoría de las charlas y sesiones se
han grabado y se han retransmitido en directo, y los vídeos están
disponibles en el
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/">archivo web de encuentros Debian</a>.
Casi todas las sesiones han permitido la participación remota a través de aplicaciones de mensajería
IRC o de documentos de texto colaborativos.
</p>

<p>
El <a href="https://debconf22.debconf.org/">sitio web DebConf22</a>
permanecerá activo como archivo y continuará ofreciendo
enlaces a las presentaciones y vídeos de charlas y eventos.
</p>

<p>
El próximo año, <a href="https://wiki.debian.org/DebConf/23">DebConf23</a> tendrá lugar en Cochín, la India,
entre el 10 y el 16 de septiembre de 2023.
Siguiendo la tradición, antes de la DebConf los organizadores y organizadoras locales de la India 
darán comienzo a las actividades de la conferencia con un DebCamp (del 3 al 9 de septiembre de 2023)
centrado en el trabajo individual y de equipo para la mejora de la
distribución.
</p>

<p>
La DebConf está comprometida con el establecimiento de un ambiente seguro y acogedor para todos y todas las participantes.
Consulte la <a href="https://debconf22.debconf.org/about/coc/">página sobre el código de conducta en el sitio web de la DebConf22</a>
para más detalles sobre esta cuestión.
</p>

<p>
Debian agradece el compromiso de numerosos <a href="https://debconf22.debconf.org/sponsors/">patrocinadores</a>
por su apoyo a la DebConf22, especialmente el de nuestros patrocinadores platino:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://itp-prizren.com/">ITP Prizren</a>
y <a href="https://google.com/">Google</a>

</p>

<h2>Acerca de Debian</h2>

<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser
un proyecto comunitario verdaderamente libre. Desde entonces el proyecto
ha crecido hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios de todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido a 70 idiomas y soporta
una gran cantidad de arquitecturas de ordenadores, por lo que el proyecto
se refiere a sí mismo como <q>el sistema operativo universal</q>.
</p>

<h2>Acerca de DebConf</h2>

<p>
DebConf es la conferencia de desarrolladores del proyecto Debian. Además de un
amplio programa de charlas técnicas, sociales y sobre reglamentación, DebConf proporciona una
oportunidad para que desarrolladores, contribuidores y otras personas interesadas se
conozcan en persona y colaboren más estrechamente. Se ha celebrado
anualmente desde 2000 en lugares tan diversos como Escocia, Argentina y
Bosnia-Herzegovina. Hay más información sobre DebConf disponible en
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Acerca de Lenovo</h2>
<p>
Como líder tecnológico global en la fabricación de un amplio catálogo de productos conectados,
incluyendo teléfonos inteligentes, tabletas, PC y estaciones de trabajo, dispositivos de realidad aumentada y de realidad virtual,
soluciones de hogar, oficina y centros de datos inteligentes, <a href="https://www.lenovo.com">Lenovo</a>
entiende lo críticos que son los sistemas y plataformas abiertos para un mundo conectado.
</p>

<h2>Acerca de Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> es la mayor empresa de alojamiento web de Suiza.
También ofrece servicios de respaldo y de almacenamiento, soluciones para organizadores de eventos y
servicios bajo demanda de retransmisión en directo («live-streaming») y vídeo.
Es propietaria de sus centros de proceso de datos y de todos los elementos críticos
para la prestación de los servicios y productos que suministra
(tanto software como hardware). 
</p>

<h2>Acerca de ITP Prizren</h2>
<p>
<a href="https://itp-prizren.com/">Innovation and Training Park Prizren</a> aspira a ser
un elemento de cambio e impulso en el área de las TIC y de las industrias agroalimentarias y creativas
a través de la creación y gestión de un entorno favorable y de servicios eficientes para las PYME, 
explotando diferentes formas de innovación que puedan contribuir a que Kosovo mejore
su nivel de desarrollo industrial y de investigación, proporcionando beneficios a la economía
y a la sociedad del país en su conjunto.
</p>

<h2>Acerca de Google</h2>
<p>
<a href="https://google.com/">Google</a> es una de las mayores empresas tecnológicas del
mundo y proporciona un amplio rango de servicios y productos relacionados con Internet, tales
como tecnologías de publicidad en línea, búsqueda, computación en la nube, software y hardware.
</p>
<p>
Google ha dado soporte a Debian patrocinando la DebConf más de
diez años y es también socia de Debian patrocinando partes
de la infraestructura de integración continua de <a href="https://salsa.debian.org">Salsa</a>
en la plataforma de la nube de Google («Google Cloud Platform»).
</p>

<h2>Información de contacto</h2>

<p>Para más información, visite la página web de DebConf22 en
<a href="https://debconf22.debconf.org/">https://debconf22.debconf.org/</a>
o envíe un correo electrónico a &lt;press@debian.org&gt;.</p>
