#use wml::debian::translation-check translation="b6a020c494e8e886f401ab63178faa0db93e39da"
<define-tag pagetitle>Debian 11 actualizado: publicada la versión 11.7</define-tag>
<define-tag release_date>2023-04-29</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la séptima actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction akregator "Corrige comprobaciones de validez, incluyendo la corrección del borrado de fuentes de noticias y de carpetas">
<correction apache2 "No habilita apache2-doc.conf automáticamente; corrige regresiones en http2 y en mod_rewrite introducidas en la 2.4.56">
<correction at-spi2-core "Da al tiempo de espera en parada («stop timeout») un valor de 5 segundos, para no bloquear innecesariamente las paradas del sistema">
<correction avahi "Corrige problema de denegación de servicio local [CVE-2021-3468]">
<correction base-files "Actualizado para la versión 11.7">
<correction c-ares "Evita desbordamiento de pila y denegación de servicio [CVE-2022-4904]">
<correction clamav "Nueva versión «estable» del proyecto original; corrige posible problema de ejecución de código remoto en el analizador sintáctico de ficheros HFS+ [CVE-2023-20032] y posible fuga de información en el analizador sintáctico de ficheros DMG [CVE-2023-20052]">
<correction command-not-found "Añade nuevo componente: 'non-free-firmware', corrigiendo las actualizaciones a bookworm">
<correction containerd "Corrige problema de denegación de servicio [CVE-2023-25153]; corrige posible elevación de privilegios por una incorrecta configuración de grupos suplementarios [CVE-2023-25173]">
<correction crun "Corrige problema de elevación de capacidades debido a la incorrecta inicialización de contenedores con permisos por omisión no nulos [CVE-2022-27650]">
<correction cwltool "Añade dependencia con python3-distutils, que faltaba">
<correction debian-archive-keyring "Añade las claves de bookworm; mueve las claves de stretch al anillo de claves eliminadas">
<correction debian-installer "Incrementa la ABI del núcleo Linux a la 5.10.0-22; recompilado contra proposed-updates">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debian-ports-archive-keyring "Retrasa un año la fecha de expiración de la clave de firma 2023; añade la clave de firma 2024; mueve la clave de firma 2022 al anillo de claves eliminadas">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction duktape "Corrige problema de caída [CVE-2021-46322]">
<correction e2tools "Corrige fallo de compilación añadiendo dependencia en tiempo de compilación con e2fsprogs">
<correction erlang "Corrige problema de elusión de autenticación del cliente [CVE-2022-37026]; usa optimización -O1 para armel porque -O2 hace que erl falle con violación de acceso en algunas plataformas como, por ejemplo, en Marvell">
<correction exiv2 "Correcciones de seguridad [CVE-2021-29458 CVE-2021-29463 CVE-2021-29464 CVE-2021-29470 CVE-2021-29473 CVE-2021-29623 CVE-2021-32815 CVE-2021-34334 CVE-2021-34335 CVE-2021-3482 CVE-2021-37615 CVE-2021-37616 CVE-2021-37618 CVE-2021-37619 CVE-2021-37620 CVE-2021-37621 CVE-2021-37622 CVE-2021-37623]">
<correction flask-security "Corrige vulnerabilidad de redirección abierta [CVE-2021-23385]">
<correction flatpak "Nueva versión «estable» del proyecto original; codifica caracteres especiales para evitar su evaluación («escape») al mostrar permisos y metadatos [CVE-2023-28101]; no permite copiar/pegar por medio de TIOCLINUX ioctl cuando se ejecuta en una consola virtual de Linux [CVE-2023-28100]">
<correction galera-3 "Nueva versión «estable» del proyecto original">
<correction ghostscript "Corrige la ruta del fichero de ayuda PostScript en ps2epsi">
<correction glibc "Corrige fuga de memoria en funciones de la familia printf con cadenas de caracteres multibyte largas; corrige caída en la familia printf debida a asignaciones dependientes de la anchura/precisión; corrige violación de acceso («segfault») en la gestión del separador de millares en printf; corrige desbordamiento en la implementación AVX2 de wcsnlen al pasar de una página a la siguiente">
<correction golang-github-containers-common "Corrige análisis sintáctico de DBUS_SESSION_BUS_ADDRESS">
<correction golang-github-containers-psgo "No entra al espacio de nombres de usuario del proceso [CVE-2022-1227]">
<correction golang-github-containers-storage "Hace que funciones que antes eran internas ahora sean accesibles públicamente, necesario para permitir la corrección de CVE-2022-1227 en otros paquetes">
<correction golang-github-prometheus-exporter-toolkit "Parchea las pruebas para evitar condiciones de carrera; corrige problema de envenenamiento de la caché de autenticación [CVE-2022-46146]">
<correction grep "Corrige coincidencia incorrecta cuando el último de múltiples patrones incluye una referencia inversa">
<correction gtk+3.0 "Corrige Wayland + EGL en plataformas que solo soportan GLES">
<correction guix "Corrige fallo de compilación por claves expiradas en el juego de pruebas">
<correction intel-microcode "Nueva versión del proyecto original para corrección de errores">
<correction isc-dhcp "Corrige el tratamiento del tiempo de vida de las direcciones IPv6">
<correction jersey1 "Corrige fallo de compilación con libjettison-java 1.5.3">
<correction joblib "Corrige problema de ejecución de código arbitrario [CVE-2022-21797]">
<correction lemonldap-ng "Corrige problema de elusión de validación de URL; corrige problema de doble factor de autenticación cuando se usa el gestor AuthBasic [CVE-2023-28862]">
<correction libapache2-mod-auth-openidc "Corrige problema de redirección abierta [CVE-2022-23527]">
<correction libapreq2 "Corrige problema de desbordamiento de memoria [CVE-2022-22728]">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libexplain "Mejora compatibilidad con versiones más recientes del núcleo: Linux 5.11 ya no tiene if_frad.h, termiox eliminado desde la versión 5.12">
<correction libgit2 "Habilita la verificación de las claves SSH, por omisión [CVE-2023-22742]">
<correction libpod "Corrige problema de elevación de privilegios [CVE-2022-1227]; corrige problema de elevación de capacidades debido al arranque incorrecto de contenedores con permisos por omisión no nulos [CVE-2022-27649]; corrige análisis sintáctico de DBUS_SESSION_BUS_ADDRESS">
<correction libreoffice "Cambia la moneda por omisión de Croacia, pasando a ser el euro; evita -Djava.class.path= vacía [CVE-2022-38745]">
<correction libvirt "Corrige problemas relacionados con el reinicio de contenedores; corrige fallos en las pruebas en combinación con versiones más recientes de Xen">
<correction libxpm "Corrige problemas de bucles infinitos [CVE-2022-44617 CVE-2022-46285]; corrige problema de «doble liberación» en código de tratamiento de errores; corrige <q>compression commands depend on PATH</q> («órdenes de compresión dependen de PATH») [CVE-2022-4883]">
<correction libzen "Corrige problema de desreferencia de puntero nulo [CVE-2020-36646]">
<correction linux "Nueva versión «estable» del proyecto original; incrementa la ABI a la 22; [rt] actualiza a la 5.10.176-rt86">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 22; [rt] actualiza a la 5.10.176-rt86">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 22; [rt] actualiza a la 5.10.176-rt86">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 22; [rt] actualiza a la 5.10.176-rt86">
<correction lxc "Corrige oráculo de existencia de ficheros [CVE-2022-47952]">
<correction macromoleculebuilder "Corrige fallo de compilación añadiendo dependencia en tiempo de compilación con docbook-xsl">
<correction mariadb-10.5 "Nueva versión «estable» del proyecto original; revierte cambio del proyecto original en la API libmariadb">
<correction mono "Elimina fichero desktop">
<correction ncurses "Se protege de datos de terminfo corruptos [CVE-2022-29458]; corrige caída de tic en sentencias tc/use muy largas">
<correction needrestart "Corrige avisos cuando se usa la opción <q>-b</q>">
<correction node-cookiejar "Se protege de cookies con tamaños maliciosamente grandes [CVE-2022-25901]">
<correction node-webpack "Evita acceso a objetos entre dominios («realm») [CVE-2023-28154]">
<correction nvidia-graphics-drivers "Nueva versión del proyecto original; correcciones de seguridad [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-450 "Nueva versión del proyecto original; correcciones de seguridad [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-470 "Nueva versión del proyecto original; correcciones de seguridad [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-modprobe "Nueva versión del proyecto original">
<correction openvswitch "Corrige <q>openvswitch-switch update leaves interfaces down</q> («la actualización de openvswitch-switch deja las interfaces en estado down»)">
<correction passenger "Corrige compatibilidad con versiones más recientes de NodeJS">
<correction phyx "Elimina dependencia en tiempo de compilación con libatlas-cpp, que es innecesaria">
<correction postfix "Nueva versión «estable» del proyecto original">
<correction postgis "Corrige orden incorrecto de ejes en la proyección estereográfica polar">
<correction postgresql-13 "Nueva versión «estable» del proyecto original; corrige problema de revelación de contenido de la memoria del cliente [CVE-2022-41862]">
<correction python-acme "Corrige la versión de los CSR creados para evitar problemas con las implementaciones de la API ACME que cumplen estrictamente la RFC">
<correction ruby-aws-sdk-core "Corrige la generación del fichero de versión">
<correction ruby-cfpropertylist "Corrige algunas funcionalidades abandonando la compatibilidad con Ruby 1.8">
<correction shim "Nueva versión del proyecto original; nueva versión «estable» del proyecto original; habilita soporte de NX en tiempo de compilación; bloquea binarios de grub de Debian con sbat &lt; 4">
<correction shim-helpers-amd64-signed "Nueva versión «estable» del proyecto original; habilita soporte de NX en tiempo de compilación; bloquea binarios de grub de Debian con sbat &lt; 4">
<correction shim-helpers-arm64-signed "Nueva versión «estable» del proyecto original; habilita soporte de NX en tiempo de compilación; bloquea binarios de grub de Debian con sbat &lt; 4">
<correction shim-helpers-i386-signed "Nueva versión «estable» del proyecto original; habilita soporte de NX en tiempo de compilación; bloquea binarios de grub de Debian con sbat &lt; 4">
<correction shim-signed "Nueva versión «estable» del proyecto original; habilita soporte de NX en tiempo de compilación; bloquea binarios de grub de Debian con sbat &lt; 4">
<correction snakeyaml "Corrige problemas de denegación de servicio [CVE-2022-25857 CVE-2022-38749 CVE-2022-38750 CVE-2022-38751]; añade documentación sobre soporte/problemas de seguridad">
<correction spyder "Corrige duplicación de código al grabar">
<correction symfony "Elimina cabeceras privadas antes de almacenar respuestas con HttpCache [CVE-2022-24894]; elimina tokens CSRF del almacenamiento en los accesos («login») satisfactorios [CVE-2022-24895]">
<correction systemd "Corrige problema de fuga de información [CVE-2022-4415] y problema de denegación de servicio [CVE-2022-3821]; ata_id: corrige la obtención del Response Code de SCSI Sense Data; logind: corrige la obtención de la propiedad OnExternalPower vía D-Bus; corrige caída en systemd-machined">
<correction tomcat9 "Añade soporte de OpenJDK 17 en la detección de JDK">
<correction traceroute "Interpreta direcciones v4mapped-IPv6 como IPv4">
<correction tzdata "Actualiza los datos incluidos">
<correction unbound "Corrige ataque de delegación sin respuesta («Non-Responsive Delegation Attack») [CVE-2022-3204]; corrige problema de <q>ghost domain names</q> («nombres de domino fantasma») [CVE-2022-30698 CVE-2022-30699]">
<correction usb.ids "Actualiza los datos incluidos">
<correction vagrant "Añade soporte de VirtualBox 7.0">
<correction voms-api-java "Corrige fallos de compilación inhabilitando algunas pruebas que no funcionan">
<correction w3m "Corrige problema de escritura fuera de límites [CVE-2022-38223]">
<correction x4d-icons "Corrige fallo de compilación con versiones más recientes de imagemagick">
<correction xapian-core "Evita corrupción de la base de datos al agotarse el espacio en disco">
<correction zfs-linux "Añade varias mejoras de estabilidad">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2022 5170 nodejs>
<dsa 2022 5237 firefox-esr>
<dsa 2022 5238 thunderbird>
<dsa 2022 5259 firefox-esr>
<dsa 2022 5262 thunderbird>
<dsa 2022 5282 firefox-esr>
<dsa 2022 5284 thunderbird>
<dsa 2022 5300 pngcheck>
<dsa 2022 5301 firefox-esr>
<dsa 2022 5302 chromium>
<dsa 2022 5303 thunderbird>
<dsa 2022 5304 xorg-server>
<dsa 2022 5305 libksba>
<dsa 2022 5306 gerbv>
<dsa 2022 5307 libcommons-net-java>
<dsa 2022 5308 webkit2gtk>
<dsa 2022 5309 wpewebkit>
<dsa 2022 5310 ruby-image-processing>
<dsa 2023 5311 trafficserver>
<dsa 2023 5312 libjettison-java>
<dsa 2023 5313 hsqldb>
<dsa 2023 5314 emacs>
<dsa 2023 5315 libxstream-java>
<dsa 2023 5316 netty>
<dsa 2023 5317 chromium>
<dsa 2023 5318 lava>
<dsa 2023 5319 openvswitch>
<dsa 2023 5320 tor>
<dsa 2023 5321 sudo>
<dsa 2023 5322 firefox-esr>
<dsa 2023 5323 libitext5-java>
<dsa 2023 5324 linux-signed-amd64>
<dsa 2023 5324 linux-signed-arm64>
<dsa 2023 5324 linux-signed-i386>
<dsa 2023 5324 linux>
<dsa 2023 5325 spip>
<dsa 2023 5326 nodejs>
<dsa 2023 5327 swift>
<dsa 2023 5328 chromium>
<dsa 2023 5329 bind9>
<dsa 2023 5330 curl>
<dsa 2023 5331 openjdk-11>
<dsa 2023 5332 git>
<dsa 2023 5333 tiff>
<dsa 2023 5334 varnish>
<dsa 2023 5335 openjdk-17>
<dsa 2023 5336 glance>
<dsa 2023 5337 nova>
<dsa 2023 5338 cinder>
<dsa 2023 5339 libhtml-stripscripts-perl>
<dsa 2023 5340 webkit2gtk>
<dsa 2023 5341 wpewebkit>
<dsa 2023 5342 xorg-server>
<dsa 2023 5343 openssl>
<dsa 2023 5344 heimdal>
<dsa 2023 5345 chromium>
<dsa 2023 5346 libde265>
<dsa 2023 5347 imagemagick>
<dsa 2023 5348 haproxy>
<dsa 2023 5349 gnutls28>
<dsa 2023 5350 firefox-esr>
<dsa 2023 5351 webkit2gtk>
<dsa 2023 5352 wpewebkit>
<dsa 2023 5353 nss>
<dsa 2023 5355 thunderbird>
<dsa 2023 5356 sox>
<dsa 2023 5357 git>
<dsa 2023 5358 asterisk>
<dsa 2023 5359 chromium>
<dsa 2023 5361 tiff>
<dsa 2023 5362 frr>
<dsa 2023 5363 php7.4>
<dsa 2023 5364 apr-util>
<dsa 2023 5365 curl>
<dsa 2023 5366 multipath-tools>
<dsa 2023 5367 spip>
<dsa 2023 5368 libreswan>
<dsa 2023 5369 syslog-ng>
<dsa 2023 5370 apr>
<dsa 2023 5371 chromium>
<dsa 2023 5372 rails>
<dsa 2023 5373 node-sqlite3>
<dsa 2023 5374 firefox-esr>
<dsa 2023 5375 thunderbird>
<dsa 2023 5376 apache2>
<dsa 2023 5377 chromium>
<dsa 2023 5378 xen>
<dsa 2023 5379 dino-im>
<dsa 2023 5380 xorg-server>
<dsa 2023 5381 tomcat9>
<dsa 2023 5382 cairosvg>
<dsa 2023 5383 ghostscript>
<dsa 2023 5384 openimageio>
<dsa 2023 5385 firefox-esr>
<dsa 2023 5386 chromium>
<dsa 2023 5387 openvswitch>
<dsa 2023 5388 haproxy>
<dsa 2023 5389 rails>
<dsa 2023 5390 chromium>
<dsa 2023 5391 libxml2>
<dsa 2023 5392 thunderbird>
<dsa 2023 5393 chromium>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction bind-dyndb-ldap "Roto con versiones más recientes de bind9; no se le puede dar soporte en «estable»">
<correction matrix-mirage "Depende de python-matrix-nio, que se elimina">
<correction pantalaimon "Depende de python-matrix-nio, que se elimina">
<correction python-matrix-nio "Problemas de seguridad; no funciona con los servidores Matrix actuales">
<correction weechat-matrix "Depende de python-matrix-nio, que se elimina">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


