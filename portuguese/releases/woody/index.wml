#use wml::debian::template title="Informações de lançamento do Debian GNU/Linux 3.0 &ldquo;woody&rdquo;" BARETITLE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6933c146d6b5dc1ef12d968ac264529d8ab5df51"

<p>O Debian GNU/Linux 3.0 (codinome <em>woody</em>) foi lançado em 19 de julho
de 2002. A nova versão inclui muitas mudanças importantes, descritas em nosso
<a href="$(HOME)/News/2002/20020719">comunicado à imprensa</a> e nas
<a href="releasenotes">notas de lançamento</a> .</p>

<p><strong>O Debian GNU/Linux 3.0 foi substituído pelo
<a href="../sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>.
Atualizações de segurança foram descontinuadas no final de junho de 2006.
</strong></p>

<p>O Debian GNU/Linux 3.0 está disponível <a href="$(DISTRIB)/">na Internet</a>
e <a href="$(HOME)/CD/vendors/">por meio de fornecedores(as) de CDs</a>.</p>

<p>Antes de instalar o Debian, por favor, leia o guia de instalação. O guia
de instalação da sua arquitetura escolhida contém instruções e links para todos
os arquivos que você precisa para instalar.</p>

<p>As seguintes arquiteturas de computador foram suportadas nesse lançamento:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mips/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Apesar de nossos desejos, podem existir alguns problemas na versão woody,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode <a href="../reportingbugs">relatar outros problemas</a>
para nós.</p>
