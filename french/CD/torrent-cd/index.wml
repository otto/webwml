#use wml::debian::cdimage title="Télécharger des images de USB/CD/DVD de Debian avec BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e" maintainer="Jean-Paul Guillonneau"
# Initial translation: Willy Picard
# Previous translation by Thomas Huriaux
# Frédéric Bothamy

<p><a href="https://fr.wikipedia.org/wiki/BitTorrent">BitTorrent</a>
est un système de téléchargement pair à pair (peer-to-peer)
optimisé pour un grand nombre d'utilisateurs. Il impose une charge minimale
à nos serveurs, car les clients BitTorrent rendent accessibles des segments de
fichier aux autres clients tout en téléchargeant, distribuant la charge
sur le réseau et permettant des téléchargements très rapides.
</p>

<div class="tip">
<p>Le <strong>premier</strong> disque USB/CD/DVD contient
tous les fichiers nécessaires pour installer un système Debian
standard.<br />
</p>
</div>

<p>
Vous avez besoin d'un client BitTorrent afin de télécharger les images
de USB/CD/DVD de Debian par BitTorrent. La distribution Debian
inclut les outils
<a href="https://packages.debian.org/aria2">aria2</a>,
<a href="https://packages.debian.org/transmission">transmission</a> ou
<a href="https://packages.debian.org/ktorrent">KTorrent</a>.
D’autres systèmes d’exploitation (tels Windows et macOS) sont pris en charge par
<a href="https://www.qbittorrent.org/download">qBittorrent</a> et
<a href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>


<h3>Fichiers bittorrent officiels pour la distribution <q>stable</q></h3>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>Assurez-vous de lire la documentation avant de faire l'installation.
<strong>Si vous ne devez lire qu'un seul document</strong> avant
l'installation, veuillez lire notre
<a href="$(HOME)/releases/stable/amd64/apa">guide d'installation</a>, un
parcours rapide du processus d'installation. D'autres documentations
utiles&nbsp;:</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">le manuel d'installation</a>,
    les instructions détaillées d'installation&nbsp;;</li>
<li><a href="https://wiki.debian.org/DebianInstaller">la documentation de
    l'installateur Debian</a>, y compris la FAQ avec des questions et
    réponses récurrentes&nbsp;;</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">les
    errata de l'installateur Debian</a>, la liste des problèmes connus
    dans l'installateur.</li>
</ul>

#<h3>Fichiers bittorrent officiels pour la distribution <q>testing</q></h3>
#
#<ul>
#
#  <li><strong>CD</strong>&nbsp;:<br>
#  <full-cd-torrent>
#  </li>
#
#  <li><strong>DVD</strong>&nbsp;;<br>
#  <full-dvd-torrent>
#  </li>
#
#</ul>

<p>
Si vous le pouvez, veuillez laisser tourner votre client après la fin de votre
téléchargement afin de permettre aux autres utilisateurs de télécharger
leurs images plus rapidement&nbsp;!
</p>
