#use wml::debian::template title="Le coin du développeur Debian" BARETITLE="true"
#use wml::debian::translation-check translation="863737f3da70734c4736c77d37da662f02421e13" maintainer="Jean-Pierre Giraud"

# Original translation by Christian Couder, 2000-2001
# Previous translator, Pierre Machard 2003-2005
# Previous translator, Nicolas Bertolissio 2006-2008
# Previous translator, David Prévot, 2010-2012
# Previous translator, Jean-Paul Guillonneau, 2018-2021

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Bien que toutes les
informations et les liens vers d’autres pages soient publiques, ce site est
principalement destiné aux développeurs Debian.</p>
</aside>

<ul class="toc">
  <li><a href="#basic">Bases</a></li>
  <li><a href="#packaging">Empaquetage</a></li>
  <li><a href="#workinprogress">Travail en cours</a></li>
  <li><a href="#projects">Projets</a></li>
  <li><a href="#miscellaneous">Divers</a></li>
</ul>


<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Informations générales</a></h2>
      <p>Liste des développeurs et responsables actuels, comment rejoindre
le projet et des liens vers la base de données des développeurs, la
constitution, le processus de vote, les publications et les architectures.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Organisation de Debian</a></dt>
        <dd>Plus d’un millier de bénévoles font partie du projet Debian.
Cette page décrit la structure organisationnelle de Debian, liste les équipes
et leurs membres ainsi que leur adresse de contact.</dd>
        <dt><a href="$(HOME)/intro/people">Les acteurs de Debian</a></dt>
        <dd>Les <a href="https://wiki.debian.org/DebianDeveloper">développeurs
Debian (DD)</a> (membres à part entière du projet Debian) et
les <a href="https://wiki.debian.org/DebianMaintainer">responsables Debian (DM)</a>,
contribuent au projet. Veuillez consulter la
<a href="https://nm.debian.org/public/people/dd_all/">liste des développeurs
de Debian</a> et la <a href="https://nm.debian.org/public/people/dm_all/">liste
des responsables de Debian</a> pour mieux connaître les personnes impliquées.
Il existe aussi une <a href="developers.loc">carte du monde des développeurs</a>.</dd>

        <dt><a href="join/">Comment rejoindre Debian</a></dt>
        <dd>Vous voulez contribuer et rejoindre le projet ? Nous sommes
toujours à la recherche de nouveaux développeurs et d’adeptes du logiciel libre
ayant des compétences techniques ou autres. Pour plus d’informations, veuillez
parcourir cette page.</dd>

        <dt><a href="https://db.debian.org/">Base de données des développeurs</a></dt>
        <dd>Certaines informations sont disponibles pour tous, certaines sont
réservées aux développeurs qui se sont enregistrés. Cette base de données
contient des informations telles que les
<a href="https://db.debian.org/machines.cgi">machines du projet</a> et les
clés OpenPGP des développeurs.
        Pour extraire la clé d'un développeur, cliquez sur le(s) lien(s)
<q>PGP/GPG fingerprint</q> quand vous l'avez trouvé.
        Les développeurs peuvent
<a href="https://db.debian.org/password.html">modifier leur mot de passe</a>
et configurer la
<a href="https://db.debian.org/forward.html">redirection du courriel</a> pour
leur compte Debian. Si vous prévoyez d’utiliser une des machines de Debian,
veuillez d’abord lire la <a href="dmup">politique d’utilisation</a> des
machines de Debian.</dd>

        <dt><a href="constitution">Constitution</a></dt>
        <dd>Ce document décrit la structure organisationnelle pour les prises
de décision formelles dans le projet.</dd>

        <dt><a href="$(HOME)/vote/">Informations sur les votes</a></dt>
        <dd>Tout ce que vous voulez savoir sur la façon dont nous élisons nos
responsables, choisissons nos logos et, de façon plus générale, comment
nous votons.</dd>

        <dt><a href="$(HOME)/releases/">Distributions</a></dt>
        <dd>Cette page liste les distributions actuelles
(<a href="$(HOME)/releases/stable/">stable</a>,
<a href="$(HOME)/releases/testing/">testing</a> et
<a href="$(HOME)/releases/unstable/">unstable</a>) et contient une liste des
anciennes distributions et leurs noms de code.</dd>

        <dt><a href="$(HOME)/ports/">Architectures différentes</a></dt>
        <dd>Debian fonctionne sur beaucoup d’architectures différentes. Cette
page rassemble des informations sur divers portages de Debian, certains basés
sur le noyau Linux, d’autres sur des noyaux de FreeBSD, NetBSD et Hurd.</dd>

     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Empaquetage</a></h2>
      <p>Liens vers notre manuel de politique et d’autres documents relatifs
à la politique, aux procédures et d’autres ressources pour les développeurs
de Debian, ainsi que le guide du nouveau responsable.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Manuel sur la politique de Debian</a></dt>
        <dd>Ce manuel décrit les exigences pour la distribution de Debian.
Cela inclut la structure et le contenu d’une archive de Debian, plusieurs
problèmes de conception ainsi que les exigences techniques que chaque paquet
doit satisfaire pour être inclus dans la distribution.

        <p>En bref, il <strong>doit</strong> être lu.</p>

        <p>Voir aussi les <a href="https://bugs.debian.org/debian-policy">les
        amendements proposés</a> pour la politique.</p>
        </dd>
      </dl>

      <p>Plusieurs autres documents concernant la politique peuvent être
intéressants :</p>
      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Norme de
         la hiérarchie des systèmes de fichiers</a> (FHS)
        <br />Cette norme est une liste des répertoires (ou fichiers) dans
         lesquels il faut placer les autres répertoires ou fichiers, et la
         compatibilité avec cette norme est requise par la Charte dans sa
         version 3.x (consultez le <a
        href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">\
        chapitre 9</a> du manuel de politique de Debian).</li>
        <li>Liste des <a href="$(DOC)/packaging-manuals/build-essential">\
        paquets nécessaires à la construction</a>
        <br />Ces paquets sont les paquets nécessaires pour compiler un logiciel,
        construire un paquet ou un ensemble de paquets. Il est nul besoin
        de les inclure dans la ligne <code>Build-Depends</code> lors de la
        <a href="https://www.debian.org/doc/debian-policy/ch-relationships.html">\
        déclaration de dépendances</a> entre paquets.</li>
        <li>Liste des <a href="$(DOC)/packaging-manuals/virtual-package-names-list.yaml">\
        noms de paquets virtuels</a></li>
        <li><a href="$(DOC)/packaging-manuals/copyright-format/1.0/">Spécification
        pour le format des copyrights</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">\
        Spécifications pour Debconf</a></li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Système de menu</a>
        <br />Structure des entrées de menu de Debian. Consultez aussi la documentation
        sur le <a href="$(DOC)/packaging-manuals/menu.html/">système de menu</a>.</li>
      </ul>

      <p>Plusieurs langages de programmation ont leur propre politique spécifique
         d'empaquetage :</p>

      <ul>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">\
        Politique pour Ada</a></li>
        <li><a href="https://wiki.debian.org/Clojure/PackagingReference">\
        Politique pour Clojure</a></li>
        <li><a href="https://wiki.debian.org/D/Policy">Politique pour D</a></li>
        <li><a href="https://go-team.pages.debian.net/packaging.html">\
        Politique pour Golang</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Politique pour
        Java</a></li>
        <li><a href="https://wiki.debian.org/Javascript/Policy">Politique pour
        Javascript</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Politique pour
        Perl</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Politique pour
        Python</a></li>
        <li><a href="https://wiki.debian.org/Teams/RustPackaging/Policy">\
        Politique pour Rust</a></li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">\
        Politique pour Tcl/Tk</a> (ébauche)</li>
      </ul>

      <p>Plusieurs programmes ou plateforme de développement ont leur propre
         politique spécifique d'empaquetage :</p>

      <ul>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Politique
        pour les applications de base de données</a> (ébauche)</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">\
        Politique pour Emacs</a></li>
      </ul>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">Référence pour les
        développeurs</a></dt>

        <dd>
        Aperçu des procédures recommandées et des ressources disponibles pour
        les développeurs de Debian — un autre <strong>doit être lu</strong>.
        </dd>

        <dt><a href="$(DOC)/manuals/debmake-doc/">Guide pour les responsables de paquet Debian</a></dt>

        <dd>
        Comment construire un paquet de Debian (dans un langage courant),
        incluant de nombreux exemples. Pour devenir un développeur ou
        responsable, c’est un bon point de départ.</dd>
      </dl>

    </div>

  </div>

</div>


<h2><a id="workinprogress">Travail en cours : liens pour les développeurs et
responsables actifs</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Distribution de test de Debian</a></dt>
  <dd>
    Générée automatiquement à partir de la distribution « unstable » :
    c’est l’emplacement où déposer les paquets pour qu’ils soient pris en
    compte dans la prochaine publication de Debian.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Release Critical Bugs</a></dt>
  <dd>
   C'est une liste des bogues qui peuvent entraîner le retrait d'un paquet de
   la distribution de test, ou même dans certains cas, provoquer un délai dans
   la sortie de la distribution. Les rapports de bogues ayant une gravité plus
   haute ou égale à « serious » sont mis dans la liste — corrigez aussi
   rapidement que possible de tels bogues dans vos paquets.
  </dd>

  <dt><a href="$(HOME)/Bugs/">Suivi de paquets (Debian Bug Tracking System — BTS)</a></dt>
    <dd>
    Pour le rapport, la discussion et la correction des bogues. Le BTS est
    utile aux utilisateurs comme aux développeurs.
    </dd>

  <dt>Informations sur les paquets Debian</dt>
    <dd>
    Les pages web sur les <a href="https://qa.debian.org/developer.php">\
    informations de paquet</a> et le
    <a href="https://tracker.debian.org/">suivi de paquet</a> fournissent
    un ensemble d’informations utiles pour les responsables. Les développeurs
    voulant suivre des paquets peuvent souscrire (par courriel) à un service
    envoyant des copies de courriels du BTS et des notifications concernant
    des téléversements et des installations. Consultez le
   <a href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">\
   manuel du suivi de paquet</a> pour plus d’informations.
    </dd>

    <dt><a href="wnpp/">Paquets ayant besoin d'aide</a></dt>
      <dd>
     La liste des paquets ayant besoin d'aide et des paquets prospectifs
     (Work-Needing and Prospective Packages — WNPP) de Debian est une
     énumération de paquets ayant besoin d’un nouveau responsable et des
     paquets qui n’ont pas encore été inclus dans Debian.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
    Système d’arrivée</a></dt>
      <dd>
      Les nouveaux paquets sont envoyés dans le système d'arrivée de nos
      serveurs d'archives. Les paquets acceptés sont disponibles par HTTP
      presqu'immédiatement à l’aide d’un navigateur web et propagés vers des
      <a href="$(HOME)/mirror/">miroirs</a> quatre fois par jour.
       <br />
      <strong>Remarque</strong> : à cause de la nature de ce système, nous ne
      recommandons pas d'en faire un miroir.</dd>

    <dt><a href="https://udd.debian.org/lintian/">Rapports Lintian</a></dt>
      <dd>
      <a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>
      est un programme qui vérifie si un paquet se conforme bien à la Charte
      Debian. Les développeurs doivent l'utiliser avant chaque mise à jour.</dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">\
     Distribution « Experimental »</a></dt>
      <dd>
      La distribution expérimentale est utilisée comme zone d'entraînement pour
      des logiciels très expérimentaux. N'utilisez les
      <a href="https://packages.debian.org/experimental/">paquets expérimentaux</a>
      que si vous savez parfaitement comment utiliser la distribution
      « instable »</dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Wiki de Debian</a></dt>
      <dd>
      Le wiki de Debian offre des conseils pour les développeurs et les autres
      contributeurs.</dd>
</dl>

<h2><a id="projects">Projets : groupes et projet internes</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Pages web de Debian</a></li>
<li><a href="https://ftp-master.debian.org/">Archives de Debian</a></li>
<li><a href="$(DOC)/ddp">Projet de documentation de Debian (DDP)</a></li>
<li><a href="https://qa.debian.org/">Équipe d’assurance qualité (QA)</a></li>
<li><a href="buildd/">Réseau d’autoconstruction</a> et ses <a href="https://buildd.debian.org/">journaux de construction</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Projet de traduction des descriptions de paquet (DDTP)</a></li>
<li><a href="debian-installer/">Installateur de Debian</a></li>
<li><a href="debian-live/">Images autonomes de Debian </a></li>
<li><a href="$(HOME)/blends/">Projet d’assemblages de Debian « Pure Blends »</a></li>

</ul>


<h2><a id="miscellaneous">Liens divers</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://peertube.debian.social/home">Enregistrements</a> de nos conférences sur PeerTube ou en se servant 
    <a href="https://debconf-video-team.pages.debian.net/videoplayer/">d'une interface utilisateur différente</a></li>
<li><a href="passwordlessssh">Configuration de SSH</a> pour ne plus demander de mot de passe</li>
<li><a href="$(HOME)/doc/debian-policy/autopkgtest.txt">Manuel d'autopkgtest</a></li>
<li>Informations sur la <a href="$(HOME)/mirror/">création d’un miroir</a> de Debian</li>
<li><a href="https://ftp-master.debian.org/new.html">Nouveaux paquets</a> en attente d’être inclus dans Debian (file NEW)</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Nouveaux paquets</a> de Debian pour les sept derniers jours</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Paquets retirés de Debian</a></li>
</ul>
