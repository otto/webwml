<define-tag pagetitle>Debian 12 <q>Bookworm</q> vydán</define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="5fc831c6a77ce47c0c4e44da8a3b594b8280e3dc" maintainer="Miroslav Kuře"

<p>
Projekt Debian po 1 roce, 9 měsících a 28 dnech vývoje s hrdostí
představuje novou stabilní verzi 12, kódovým názvem <q>Bookworm</q>.
</p>

<p>
Díky spojenému úsilí
týmů <a href="https://security-team.debian.org/">Debian Security</a>
a <a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>
bude tato verze podporována 5 let.
</p>

<p>
Na základě
<a href="$(HOME)/vote/2022/vote_003">Obecného usnesení ohledně
nesvobodného firmwaru</a> jsme v archivu vytvořili novou sekci, která
umožňuje oddělit nesvobodný firmware od ostatních nesvobodných balíků:</p>
<ul>
<li>non-free-firmware</li>
</ul>

<p>Většina balíků s nesvobodným firmwarem byla přesunuta
z <b>non-free</b> do <b>non-free-firmware</b>. Toto oddělení umožňuje
vytvářet různé verze oficiálních instalačních médií.
</p>

<p>
Debian 12 <q>Bookworm</q> obsahuje různá desktopová prostředí, jako
například:
</p>
<ul>
<li>Gnome 43,</li>
<li>KDE Plasma 5.27,</li>
<li>LXDE 11,</li>
<li>LXQt 1.2.0,</li>
<li>MATE 1.26,</li>
<li>Xfce 4.18</li>
</ul>

<p>
Z celkového počtu <b>64 419</b> balíků v tomto vydání je <b>11 089</b>
balíků zcela nových, <b>6 296</b> balíků bylo odstraněno
jako <q>zastaralé</q> a <b>43 254</b> balíků bylo aktualizováno.
Komplení <q>Bookworm</q> zabírá <b>365 016 420 kB (365 GB)</b>
diskového prostoru a tvoří jej <b>1 341 564 204</b> řádků kódu.
</p>

<p>
<q>Bookworm</q> má více přeložených manuálových stránek než kdykoliv
dříve díky překladatelům, kteří <b>man</b>uálové stránky překládají
také do: češtiny, dánštiny, řečtiny, finštiny, indonéštiny,
makedonštiny, norštiny (Bokmål), ruštiny, srbštiny, švédštiny,
ukrajinštiny a vietnamštiny. Německý překlad všech manuálových
stránek <b>systemd</b> je nyní komplení.
</p>

<p>
Varianta Debianu Debian Med přináší nový balík: <b>shiny-server</b>,
který zjednodušuje vědecké webové aplikace používající <b>R</b>.
Držíme se našich snah o kontinuální integraci balíků spravovaných
týmem Debian Med, které si nyní můžete nainstalovat pomocí metabalíků
ve verzi 3.8.x.
</p>

<p>
Varianta Debianu Debian Astro zůstává kompletním řešením na klíč pro
profesionální astronomy, nadšence a hobisty. Aktualizovány byly téměř
všechny verze zahrnutých balíků. Z nových balíků zmiňme třeba <b>astap</b> a
<b>planetary-system-stacker</b>, které pomáhají se skládáním obrázků a
astrometrií, nebo korelátor <b>openvlbi</b>.
</p>

<p>
Na platformu ARM64 se vrátila podpora pro Secure Boot: uživatelé ARM64
hardwaru podporujícícho UEFI nyní mohou plně využít bezpečnostního
potenciálu povoleného režimu Secure Boot.
</p>

<p>
Debian 12 <q>Bookworm</q> obsahuje množství aktualizovaných
softwarových balíků (přes 67% oproti předchozímu vydání), mezi kterými
nechybí třeba:
</p>

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS Server 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (výchozí poštovní server) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU Compiler Collection 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>GNU C Library 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Linux řady 6.1</li>
<li>LLVM/Clang 13.0.1, 14.0 (výchozí) a 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>

<p>
S tímto bohatým výběrem softwaru a tradiční podporou širokého spektra
počítačových architektur Debian opět potvrzuje svůj záměr být
<q>univerzálním operačním systémem</q>. Je vhodný pro nejrůznější způsoby
nasazení: od desktopů k netbookům, od vývojářských stanic ke klastrům
a databázovým, webovým či datovým serverům. Aby <q>Bookworm</q> splnil
vysoká očekávání, která uživatelé stabilního vydání předpokládají,
byly použity dodatečné způsoby zajištění kvality jako například
automatické testování instalací a aktualizací všech balíků v archivu.
</p>

<p>
Oficiálně je podporováno devět architektur:
</p>
<ul>
<li>32 bitové PC (i386) a 64 bitové PC (amd64),</li>
<li>64 bitový ARM (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (EABI hard-float ABI, armhf),</li>
<li>little-endian MIPS (mipsel),</li>
<li>64 bitový little-endian MIPS (mips64el),</li>
<li>64 bitové little-endian PowerPC (ppc64el),</li>
<li>IBM System z (s390x)</li>
</ul>

<p>
32 bitové PC (i386) již nadále nepodporuje žádný procesor řady i586;
nový minimální požadavek na procesor je řada i686. <i>Pokud to váš
počítač nesplňuje, doporučujeme zůstat na distribuci Bullseye po
zbytek její podpory.</i> </p>

<p>
Tým Debian Cloud vydává obrazy s <q>Bookwormem</q> pro několik
poskytovatelů cloudových služeb:
</p>
<ul>
<li>Amazon EC2 (amd64 a arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (obecné) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amd64, arm64, ppc64el)</li>
</ul>
<p>
Obraz genericcloud by měl být schopen běžet v jakémkoliv
virtualizovaném prostředí, obraz nocloud je užitečný pro testování
procesu sestavení.
</p>

<p>
GRUB nyní ve výchozím nastavení <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#grub-os-prober">nespouští os-prober pro nalezení ostatních operačních systémů.</a>
</p>

<p>
Technický výbor potvrdil, že má Debian <q>Bookworm</q>
<a href="https://wiki.debian.org/UsrMerge">podporovat pouze kořenový souborový systém
se sloučeným adresářem usr</a> a zahodit podporu pro nesloučené
rozložení. Systémů, které byly prvotně nainstalovány jako Buster nebo
Bullseye se to netýká, protože již sloučené usr používají, ale starší
systémy budou během aktualizace převedeny na nové rozložení.
</p>

<h3>Chcete Debian vyzkoušet?</h3>
<p>
Chcete-li Debian 12 <q>Bookworm</q> vyzkoušet jednoduše bez nutnosti
cokoliv instalovat, můžete využít některý
z <a href="$(HOME)/CD/live/">živých (live) obrazů</a>, který zavede
celý operační systém do operační paměti počítače, kde si ho můžete
vyzkoušet bez obav z rozbití stávajícího systému.
</p>

<p>
Živé obrazy jsou k dispozici na architekturách <code>amd64</code>
a <code>i386</code> ve formě obrazů pro DVD, USB paměti a zavádění ze
sítě. Uživatel si může vyzkoušet různá desktopová prostředí: GNOME,
KDE Plasma, LXDE, LXQt, MATE a Xfce. <q>Bookworm</q> má i standardní
živý obraz základního Debianu bez grafického uživatelského rozhraní.
</p>

<p>
Jestliže se vám bude nový operační systém líbit, máte možnost si ho
nainstalovat na disk počítače přímo z živého obrazu. Živé obrazy
obsahují jak standardní instalační systém Debianu, tak i nezávislý
instalátor Calamares. Více informací naleznete
v <a href="$(HOME)/releases/bookworm/releasenotes">poznámkách k
vydání</a> a na stránkách Debianu v
části <a href="$(HOME)/CD/live/">živé obrazy</a>.
</p>

<p>
Jestliže chcete Debian rovnou nainstalovat, můžete si
<a href="https://www.debian.org/download">stáhnout</a> nejrůznější
instalační média pro Blu-ray, DVD, CD, USB paměti, nebo třeba síť.
Více se dozvíte
v <a href="$(HOME)/releases/bookworm/installmanual">instalační
příručce</a>.
</p>

<p>
Debian je nyní možné instalovat v 78 jazycích, z nichž většina funguje
jak v textovém, tak v grafickém rozhraní.
</p>

<p>
Obrazy instalačních médií si můžete již nyní stáhnout přes
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (doporučená možnost),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> nebo
<a href="$(HOME)/CD/http-ftp/">HTTP</a> (vizte
<a href="$(HOME)/CD/">Debian na CD</a>). V brzké době
bude <q>Bookworm</q> dostupný i
u <a href="$(HOME)/CD/vendors">prodejců</a> na DVD, CD a Blu-ray
discích.
</p>


<h3>Aktualizace Debianu</h3>

<p>
Přechod na Debian 12 <q>Bookworm</q> z předchozího vydání
(Debian 11 <q>Bullseye</q>) je ve většině případů řešen automaticky
nástrojem pro správu balíků APT.
</p>

<p>
Před aktualizací systému je velmi vhodné vytvořit úplnou zálohu, nebo
přinejmenším zálohovat data a konfigurační soubory, které si nemůžete
dovolit ztratit. Nástroje pro aktualizaci systému jsou spolehlivé, ale
například selhání hardware během aktualizace by mohlo vést ke kriticky
poškozenému systému.
</p>

<p>
Klíčové asi bude zazálohovat obsah /etc, /var/lib/dpkg,
/var/lib/apt/extended_states a rovněž výstup z
<code>$ dpkg --get-selections '*' # (apostrofy jsou důležité)</code>
</p>

<p>
Vítáme jakékoliv postřehy uživatelů ohledně přechodu z <q>Bullseye</q>
na <q>Bookworm</q>. Sdílejte informace s ostatními zasláním hlášení do
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.en.html#upgrade-reports">debianího
systému sledování chyb</a> pomocí balíku <b>upgrade-reports</b>.
</p>

<p>
V instalačním systému Debianu se událo mnohé, ale zmiňme hlavně lepší
podporu hardwaru, opravy v grafické podpoře, v zavaděči písem v GRUBu,
odstranění dlouhého čekání na konci instalace a lepší rozpoznání
systémů zaváděných přes BIOS. Tato verze instalačního systému může v
případě potřeby povolit instalaci nesvobodného firmwaru.
</p>

<p>
Balík <b>ntp</b> byl nahrazen balíkem <b>ntpsec</b> s tím, že výchozí
systémová služba pro správu hodin je nyní <b>systemd-timesyncd</b>;
podporovány jsou též <b>chrony</b> a <b>openntpd</b>.
</p>


<p>
Jelikož
byl <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split"> <b>nesvobodný</b>
firmware přesunut do vlastní sekce v archivu</a>, pokud jste nějaký
takový firmware používali, je doporučeno do APT sources.list přidat
sekci <b>non-free-firmware</b>.
</p>

<p>
Před samotnou aktualizací se doporučuje odstranit z APT sources.list
záznamy odkazující na bullseye-backports; po aktualizaci můžete zvážit
přidání <b>bookworm-backports</b>.
</p>

<p>
Pro <q>Bookworm</q> se repozitář s bezpečnostními aktualizacemi
jmenuje <b>bookworm-security</b> a uživatelé by měli při aktualizaci
příslušně upravit své APT sources.list.

Jestliže ve svém nastavení APT používáte vypichování balíků nebo
<code>APT::Default-Release</code>, nejspíš to také bude vyžadovat
úpravy, abyste umožnili balíkům aktualizovat na nové stabilní vydání.
Zvažte prosím
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#disable-apt-pinning">vypnutí
vypichování</a>.
</p>

<p>
Aktualizace na OpenLDAP 2.5 obsahuje
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#openldap-2.5">nekompatibilní
změny, které mohou vyžadovat ruční zásah</a>. V závislosti na
nastavení je možné, že služba <b>slapd</b> zůstane po aktualizaci
zastavená do doby, než nastavení dokončíte.
</p>

<p>
Používáte-li službu <b>systemd-resolved</b>, budete muset příslušný
balík po aktualizaci doinstalovat ručně,
protože <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#systemd-resolved">byl
oddělen do samostatného balíku</a>. Než balík nainstalujete, je možné,
že přestane fungovat překlad DNS jmen, protože služba na systému
nebude dostupná.
</p>

<p>
Změny proběhly také v oblasti
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#changes-to-system-logging">systémového
logování</a>; balík <b>rsyslog</b> již na většině systémů není potřeba
a ve výchozím nastavení se neinstaluje. Uživatelé mohou přejít
na <b>journalctl</b>. Zůstanete-li u <b>rsyslog</b>, začnou se
používat nové <q>časové značky s vysokou přesností</q>.
</p>

<p>
U některých balíků nemůže Debian zajistit poskytování bezpečnostních
aktualizací. Více informací naleznete v
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#limited-security-support">Limity bezpečnostních aktualizací</a>.
</p>

<p>
Aktualizace Debianu může být jako vždy provedena bezbolestně, na místě
a bez zbytečné odstávky systému, ovšem důrazně doporučujeme přečíst
si <a href="$(HOME)/releases/bookworm/releasenotes">poznámky k
vydání</a>
a <a href="$(HOME)/releases/bookworm/installmanual">instalační
příručku</a> a předejít tak případným problémům. V nejbližších týdnech
po vydání se budou poznámky k vydání dále vylepšovat a překládat do
dalších jazyků.
</p>


<h2>O Debianu</h2>

<p>
Debian je svobodný operační systém vyvíjený tisícovkami dobrovolníků z
celého světa spolupracujících prostřednictvím Internetu. Hlavními
silnými stránkami projektu Debian jsou dobrovolnická základna,
dodržování společenské smlouvy Debianu (Debian Social Contract) a
odhodlání poskytovat nejlepší možný operační systém. Vydání nové verze
Debianu je v tomto směru dalším důležitým krokem.
</p>

<h2>Kontaktní informace</h2>

<p>
Pro další informace prosím navštivte webové stránky Debianu
na <a href="$(HOME)/">https://www.debian.org/</a> nebo zašlete email
na &lt;press@debian.org&gt;.
</p>

