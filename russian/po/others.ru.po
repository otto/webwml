# translation of others.ru.po to Russian
# Yuri Kozlov <yuray@komyakino.ru>, 2009, 2011, 2013.
# Lev Lamberov <l.lamberov@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml others\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-01-23 13:05+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: ru <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Уголок новых членов"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Шаг 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Шаг 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Шаг 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Шаг 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Шаг 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Шаг 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Шаг 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Обходной лист"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"См. более подробную информацию на <a href=\"m4_HOME/intl/french/\">http://"
"www.debian.org/intl/french/</a> (только на французском языке)."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Дополнительная информация"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"См. более подробную информацию на <a href=\"m4_HOME/intl/spanish/\">http://"
"www.debian.org/intl/spanish/</a> (только на испанском языке)."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Телефон"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Факс"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Адрес"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Продукция"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Футболки"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "шапки"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "наклейки"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "кружки"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "другая одежда"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "рубашки"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "фрисби"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "коврики для мыши"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "значки"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "баскетбольные корзины"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "серьги"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "дипломаты"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "зонтики"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "наволочки"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "брелоки"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Швейцарские армейские ножи"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "Карты USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "ланьярды"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "другое"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Доступные языки:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Международная доставка:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "по Европе"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "Страна:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Жертвует средства Проекту Debian"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""
"Деньги используются для организации местных мероприятий, посвящённых "
"свободному ПО"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Со&nbsp;словом&nbsp;Debian"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Без&nbsp;слова&nbsp;Debian"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Encapsulated PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Под управлением Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Под управлением Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Под управлением Debian]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (мини-кнопка)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "тоже, что и выше"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Как давно ты используешь Debian?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Ты являешься разработчиком Debian?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "В какие области Debian ты вовлечена?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Что заинтересовало тебя в работе с Debian?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"У тебя есть какие-либо советы женщинам, заинтересованным в большем участии в "
"Debian?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""
"Ты участвуешь в других женских группах, посвящённых технологии? В каких?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Немного о тебе..."

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Неизвестно"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "ALL"
#~ msgstr "Все"

#~ msgid "Architecture:"
#~ msgstr "Архитектура:"

#~ msgid "BAD"
#~ msgstr "BAD"

#~ msgid "BAD?"
#~ msgstr "BAD?"

#~ msgid "Booting"
#~ msgstr "Загружается"

#~ msgid "Building"
#~ msgstr "Собирается"

#~ msgid "Download"
#~ msgstr "Скачать"

#~ msgid "No images"
#~ msgstr "Нет образов"

#~ msgid "No kernel"
#~ msgstr "Нет ядра"

#~ msgid "Not yet"
#~ msgstr "Пока нет"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "Old banner ads"
#~ msgstr "Старые баннеры"

#~ msgid "Package"
#~ msgstr "Пакет"

#~ msgid "Specifications:"
#~ msgstr "Спецификация:"

#~ msgid "Status"
#~ msgstr "Статус"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Unavailable"
#~ msgstr "Нет в наличии"

#~ msgid "Unknown"
#~ msgstr "Неизвестно"

#~ msgid "Version"
#~ msgstr "Версия"

#~ msgid "Wanted:"
#~ msgstr "Требуется:"

#~ msgid "Where:"
#~ msgstr "Где:"

#~ msgid "Who:"
#~ msgstr "Требуется для:"

#~ msgid "Working"
#~ msgstr "Работает"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (не работает)"
