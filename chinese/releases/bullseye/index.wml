#use wml::debian::template title="Debian&ldquo;bullseye&rdquo;发行信息"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="77c9da4860132027cb6546223ff0e9b84071a5b7"

<p>Debian <current_release_bullseye> 已\
于 <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a> 发布。\
<ifneq "11.0" "<current_release>"
  "Debian 11.0 最初发布于 <:=spokendate('2021-08-14'):>。"
/>\
此次發行包含了許多重要的\
变更，在\
我們的<a href="$(HOME)/News/2021/20210814">新聞稿</a>與\
<a href="releasenotes">发行说明</a>有詳細的介绍。</p>

<p><strong>Debian 11 已被 \
<a href="../bookworm/">Debian 12（<q>bookworm</q>）取代</a>。
</strong></p>

<p>
Debian 11 的生命周期为五年：首先是三年的完整 Debian 支持，
至 <:=spokendate('2024-08-14'):>，然后是两年的长期支持（LTS），
至 <:=spokendate('2026-08-31'):>。在 Bullseye 的 LTS 支持周期中，
受支持的架构会减少，仅支持 i386、amd64、
armhf 和 arm64。要了解更多信息，
请阅读<a href="$(HOME)/security/">安全信息</a>页面
和 <a  href="https://wiki.debian.org/LTS">Debian Wiki 的 LTS 介绍页面</a>。
</p>

<p>要取得與安裝 Debian，請見\
<a href="debian-installer/">安裝資訊</a>頁面與\
<a href="installmanual">安裝指南</a>。要從較舊的
Debian 發行版升級，請見\
<a href="releasenotes">发行说明</a>的操作指引。</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>以下是 bullseye 最初发布时支持的计算机架构：</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>事與願違，發行版即使被公開宣佈是<em>穩定的（stable）</em>，\
仍可能會存在一些問題。我們已製作了\
<a href="errata">重要已知問題列表</a>，您也可以隨時\
<a href="../reportingbugs">回報其他問題</a>給我們。</p>

<p>最後值得一提的是，我們有個<a href="credits">鳴謝</a>列表，列出為此次\
發行版做出貢獻的人。</p>
