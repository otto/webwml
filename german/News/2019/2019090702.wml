<define-tag pagetitle>Debian 9 aktualisiert: 9.10 veröffentlicht</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news
# $Id:
#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7" maintainer="Erik Pfannenstein"


<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die zehnte Aktualisierung seiner 
Oldstable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Oldstable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können.
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:
</p>


<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Oldstable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung; VERSION_CODENAME in os-release eingetragen">
<correction basez "base64url-kodierte Zeichenketten richtig dekodieren">
<correction biomaj-watcher "Upgrades von Jessie auf Stretch korrigiert">
<correction c-icap-modules "Unterstützung für clamav 0.101.1 hinzugefügt">
<correction chaosreader "Fehlende Abhängigkeit von libnet-dns-perl nachgetragen">
<correction clamav "Neue Veröffentlichung der Originalautoren: Scanzeit-Begrenzung als Maßnahme gegen ZIP-Bomben [CVE-2019-12625]; Schreibzugriff außerhalb der Grenzen innerhalb der NSIS-bzip2-Bibliothek behoben [CVE-2019-12900]">
<correction corekeeper "Keine allgemein schreibbare /var/crash mit dem Dumper-Skript benutzen; ältere Versionen des Linux-Kernels auf sicherere Art handhaben; keine core-Dateinamen abschneiden, wenn die Programmnamen Leerzeichen enthalten">
<correction cups "Mehrere Sicherheits-/Offenlegungsprobleme behoben - SNMP-Pufferüberläufe [CVE-2019-8696 CVE-2019-8675], IPP-Pufferüberlauf, Dienstblockade- und Speicheroffenlegungsprobleme im Scheduler">
<correction dansguardian "Unterstützung für clamav 0.101 hinzugefügt">
<correction dar "Neubau, damit die <q>built-using</q>-Pakete aktualisiert werden">
<correction debian-archive-keyring "Wheezy- mit Buster-Schlüsseln ersetzen">
<correction fence-agents "Dienstblockade behoben [CVE-2019-10153]">
<correction fig2dev "Speicherzugriffsfehler bei Kreis-/Halbkreis-Pfeilspitzen mit einer Vergrößerung oberhalb von 42 beseitigt [CVE-2019-14275]">
<correction fribidi "Rechts-nach-links-Ausgabe im Textmodus des Debian-Installers überarbeitet">
<correction fusiondirectory "Genauere Prüfungen bei LDAP-Abfragen; fehlende Abhängigkeit von php-xml hinzugefügt">
<correction gettext "xgettext() nicht mehr abstürzen lassen, wenn es mit der Option --its=DATEI ausgeführt wird">
<correction glib2.0 "Beim Verwenden des GKeyfileSettingsBackend Verzeichnis und Datei mit strengen Berechtigungen anlegen [CVE-2019-13012]; Puffer-Leseüberlauf beim Formatieren von Fehlermeldungen wegen ungültigem UTF-8 in GMarkup verhindert [CVE-2018-16429]; NULL-Dereferenzierung beim Auswerten von ungültigem GMarkup mit einem Schluss-Tag ohne Eröffnungs-Tag [CVE-2018-16429]">
<correction gocode "gocode-auto-complete-el: Die Vor-Abhängigkeit von auto-complete-el versionieren, damit Upgrades von Jessie auf Stretch funktionieren">
<correction groonga "Privilegien-Eskalation durch Ändern des Besitzers und der Besitzergruppe der Protokolle via <q>su</q> ausgehebelt">
<correction grub2 "Korrekturen für die Xen-UEFI-Unterstützung">
<correction gsoap "Dienstblockade bei Server-Anwendung, die mit dem -DWITH_COOKIES-Schalter kompiliert worden ist, behoben [CVE-2019-7659]; Problem mit DIME-Protkoll-Empfänger und defekten DIME-Headern behoben">
<correction gthumb "Double-Free-Problem behoben [CVE-2018-18718]">
<correction havp "Unterstützung für clamav 0.101.1 hinzugefügt">
<correction icu "Speicherzugriffsfehler im pkgdata-Befehl behoben">
<correction koji "SQL-Injection behoben [CVE-2018-1002161]; SCM-Pfade richtig validieren [CVE-2017-1002153]">
<correction lemonldap-ng "Regression in der Domain-übergreifenden Authentifizierung behoben; XML-External-Entity-Anfälligkeit behoben">
<correction libcaca "Probleme mit Ganzzahlen-Überläufen behoben [CVE-2018-20545 CVE-2018-20546 CVE-2018-20547 CVE-2018-20548 CVE-2018-20549]">
<correction libclamunrar "Neue stabile Version der Originalautoren">
<correction libconvert-units-perl "Neubau ohne Änderungen, aber mit korrigierter Versionsnummer">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libebml "Korrektur der Originalautoren für Heap-basiertes Lesen außerhalb des Puffers (buffer-overread)">
<correction libevent-rpc-perl "Kompilierungsfehlschlag wegen ausgelaufenen SSL-Testzertifikaten behoben">
<correction libgd2 "Uninitialisierten Lesezugriff in gdImageCreateFromXbm behoben [CVE-2019-11038]">
<correction libgovirt "Testzertifkate mit Ablaufdatum in ferner Zukunft neu generiert, um Test-Fehlschläge zu vermeiden">
<correction librecad "Dienstblockade durch präparierte Datei behoben [CVE-2018-19105]">
<correction libsdl2-image "Mehrere Sicherheitsprobleme behoben">
<correction libthrift-java "Umgehung der SASL-Aushandlung behoben [CVE-2018-1320]">
<correction libtk-img "Keine internen Kopien der JPEG-, Zlib- und PixarLog Codecs mehr benutzen, damit die Abstürze aufhören">
<correction libu2f-host "Stack-Speicherleck behoben [CVE-2019-9578]">
<correction libxslt "Umgehung des Sicherheits-Frameworks behoben [CVE-2019-11068]; uninitialiserten Speicherzugriff auf das xsl:number-Token behoben [CVE-2019-13117]; uninitialisierten Lesezugriff mit UTF-8-Gruppierungszeichen behoben [CVE-2019-13118]">
<correction linux "Neue Version der Originalautoren mit ABI-Bump; Sicherheitskorrekturen [CVE-2015-8553 CVE-2017-5967 CVE-2018-20509 CVE-2018-20510 CVE-2018-20836 CVE-2018-5995 CVE-2019-11487 CVE-2019-3882]">
<correction linux-latest "Aktualisierung für 4.9.0-11-Kernel-ABI">
<correction liquidsoap "Kompilierung mit Ocaml 4.02 überarbeitet">
<correction llvm-toolchain-7 "Neues Paket, welches das Kompilieren neuer Firefox-Versionen unterstützt">
<correction mariadb-10.1 "Neue stabile Version der Originalautoren; Sicherheitskorrekturen [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2805 CVE-2019-2627 CVE-2019-2614]">
<correction minissdpd "Use-after-free-Anfälligkeit beseitigt, die einem Angreifer erlauben würde, den Prozess aus der Ferne abstürzen zu lassen [CVE-2019-12106]">
<correction miniupnpd "Dienstblockade-Probleme behoben [CVE-2019-12108 CVE-2019-12109 CVE-2019-12110]; Informationsleck behoben [CVE-2019-12107]">
<correction mitmproxy "Tests ausschließen, die Internetzugriff benötigen; Einfügen ungewollter Abhängigkeiten mit nach oben beschränkter Version verhindern">
<correction monkeysphere "Kompilierfehlschlag durch Überarbeiten der Tests behoben, damit diese zu einem aktualisierten GnuPG in Stretch passen, welches andere Ausgaben produziert">
<correction nasm-mozilla "Neues Paket, damit das Kompilieren neuer Firefox-Versionen unterstützt wird">
<correction ncbi-tools6 "Neubau ohne die unfreien data/UniVec.*">
<correction node-growl "Eingaben vor dem Weiterreichen an exec überprüfen">
<correction node-ws "Upload-Größe begrenzen [CVE-2016-10542]">
<correction open-vm-tools "Mögliches Sicherheitsproblem mit den Berechtigungen des dazwischenliegenden Staging-Verzeichnisses und -Pfades behoben">
<correction openldap "rootDN-proxyauthz auf seine eigenen Datenbanken beschränken [CVE-2019-13057]; sasl_ssf-ACL-Statement für jede Verbindung erzwingen [CVE-2019-13565]; slapo-rwm überarbeitet, damit es nicht den Originalfilter löscht, wenn der neu geschriebene Filter ungültig ist">
<correction openssh "Deadlock beim Schlüsselvergleich behoben">
<correction passwordsafe "Keine Lokalisierungsdateien in einem separaten Unterverzeichnis speichern">
<correction pound "Abfrage-Schmuggel via präparierten Headern behoben [CVE-2016-10711]">
<correction prelink "Neukompilierung zum Aktualisieren der <q>built-using</q>-Pakete">
<correction python-clamav "Unterstützung für clamav 0.101.1 hinzugefügt">
<correction reportbug "Update release names, following buster release">
<correction resiprocate "Resolve an installation issue with libssl-dev and --install-recommends">
<correction sash "Neukompilierung zum Aktualisieren der <q>built-using</q>-Pakete">
<correction sdl-image1.2 "Pufferüberläufe [CVE-2018-3977 CVE-2019-5058 CVE-2019-5052] und Zugriffe außerhalb der Grenzen behoben [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction signing-party "Unsicheren Shell-Aufruf behoben, der Shell-Injection via User-ID ermöglicht hätte [CVE-2019-11627]">
<correction slurm-llnl "Potenziellen Heap-Überlauf auf 32-Bit-Systemen behoben [CVE-2019-6438]">
<correction sox "Mehrere Sicherheitsprobleme behoben [CVE-2019-8354 CVE-2019-8355 CVE-2019-8356 CVE-2019-8357 927906 CVE-2019-1010004 CVE-2017-18189 881121 CVE-2017-15642 882144 CVE-2017-15372 878808 CVE-2017-15371 878809 CVE-2017-15370 878810 CVE-2017-11359 CVE-2017-11358 CVE-2017-11332">
<correction systemd "Den ndisc-Client nicht stoppen, falls die Konfiguration fehlerhaft ist">
<correction t-digest "Neukompilierung ohne Änderungen, um die Wiederverwendung der vor-epochalen Version 3.0-1 zu vermeiden">
<correction tenshi "PID-Dateiproblem behoben, welches lokalen Benutzern erlaubte, eigenmächtig Prozesse abzuschießen [CVE-2017-11746]">
<correction tzdata "Neue Veröffentlichung der Originalautoren">
<correction unzip "Fehlerhafte Auswertung von 64-Bit-Werten in fileio.c behoben; ZIP-Bomben-Probleme behoben [CVE-2019-13232]">
<correction usbutils "USB-ID-Liste aktualisiert">
<correction xymon "Mehrere (serverbezogene) Sicherheitsprobleme behoben [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubico-piv-tool "Sicherheitsprobleme behoben [CVE-2018-14779 CVE-2018-14780]">
<correction z3 "Den SONAME von libz3java.so nicht auf libz3.so.4 setzen">
<correction zfs-auto-snapshot "Cron-Jobs nach der Paketentfernung leise beenden lassen">
<correction zsh "Neukompilierung zum Aktualisieren der <q>built-using</q>-Pakete">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Oldstable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheits-Team hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2019 4435 libpng1.6>
<dsa 2019 4436 imagemagick>
<dsa 2019 4437 gst-plugins-base1.0>
<dsa 2019 4438 atftp>
<dsa 2019 4439 postgresql-9.6>
<dsa 2019 4440 bind9>
<dsa 2019 4441 symfony>
<dsa 2019 4442 cups-filters>
<dsa 2019 4442 ghostscript>
<dsa 2019 4443 samba>
<dsa 2019 4444 linux>
<dsa 2019 4445 drupal7>
<dsa 2019 4446 lemonldap-ng>
<dsa 2019 4447 intel-microcode>
<dsa 2019 4448 firefox-esr>
<dsa 2019 4449 ffmpeg>
<dsa 2019 4450 wpa>
<dsa 2019 4451 thunderbird>
<dsa 2019 4452 jackson-databind>
<dsa 2019 4453 openjdk-8>
<dsa 2019 4454 qemu>
<dsa 2019 4455 heimdal>
<dsa 2019 4456 exim4>
<dsa 2019 4457 evolution>
<dsa 2019 4458 cyrus-imapd>
<dsa 2019 4459 vlc>
<dsa 2019 4460 mediawiki>
<dsa 2019 4461 zookeeper>
<dsa 2019 4462 dbus>
<dsa 2019 4463 znc>
<dsa 2019 4464 thunderbird>
<dsa 2019 4465 linux>
<dsa 2019 4466 firefox-esr>
<dsa 2019 4467 vim>
<dsa 2019 4468 php-horde-form>
<dsa 2019 4469 libvirt>
<dsa 2019 4470 pdns>
<dsa 2019 4471 thunderbird>
<dsa 2019 4472 expat>
<dsa 2019 4473 rdesktop>
<dsa 2019 4475 openssl>
<dsa 2019 4475 openssl1.0>
<dsa 2019 4476 python-django>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4485 openjdk-8>
<dsa 2019 4487 neovim>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4492 postgresql-9.6>
<dsa 2019 4494 kconfig>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4506 qemu>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction pump "Unbetreut, Sicherheitsprobleme">
<correction teeworlds "Sicherheitsprobleme, nicht kompatibel mit aktuellen Servern">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Oldstable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständigen Listen von Paketen, die sich mit dieser Revision geändert 
haben:</p>


<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Oldstable-Distribution:</p>


<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Oldstable-Distribution:</p>


<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>


<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail an 
&lt;press@debian.org&gt; oder kontaktieren das Oldstable-Release-Team 
auf Englisch über &lt;debian-release@lists.debian.org&gt;.</p>

