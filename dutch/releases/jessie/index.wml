#use wml::debian::template title="Debian &ldquo;jessie&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ff4acdb89311338bebdbb71b28dcc479d29029e3"

<p>Debian <current_release_jessie> werd uitgebracht op
<a href="$(HOME)/News/<current_release_newsurl_jessie/>"><current_release_date_jessie></a>.
<ifneq "8.0" "<current_release>"
  "Debian 8.0 werd oorspronkelijk uitgebracht op <:=spokendate('2015-04-26'):>."
/>
De release bevatte verschillende belangrijke wijzigingen, beschreven in ons
<a href="$(HOME)/News/2015/20150426">persbericht</a> en in de
<a href="releasenotes">Notities bij de release</a>.</p>

<p><strong>Debian 8 werd vervangen door
<a href="../stretch/">Debian 9 (<q>stretch</q>)</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds <:=spokendate('2018-06-17'):>.
</strong></p>

<p><strong>Jessie heeft ook genoten van langetermijnondersteuning (Long Term Support - LTS) tot
eind juni 2020.  De LTS was beperkt tot i386, amd64, armel en armhf.
Raadpleeg voor meer informatie de <a
href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
</strong></p>

<p>Raadpleeg de installatie-informatie-pagina en de
Installatiehandleiding over het verkrijgen en installeren
van Debian. Zie de instructies in
de <a href="releasenotes">notities bij de release</a> om van een oudere Debian release
op te waarderen.</p>

<p>Ondersteunde architecturen tijdens de langetermijnondersteuning:</p>

<ul>
<li><a href="../../ports/amd64/">64-bits pc (amd64)</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
</ul>

<p>Ondersteunde computerarchitecturen bij de initiële release van Jessie:</p>

<ul>
<li><a href="../../ports/amd64/">64-bits pc (amd64)</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">64-bits ARM (AArch64)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd andere problemen rapporteren.</p>
