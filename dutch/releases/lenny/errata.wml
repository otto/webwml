#use wml::debian::template title="Debian GNU/Linux 5.0 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="256bbbf3eb181693a0ebf3daf93179f79f7e38a0"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>



<toc-add-entry name="known_probs">Bekende problemen</toc-add-entry>

<dl>
<dt>Waarschuwing over sleutel 4D270D06F42584E6 bij het opwaarderen</dt>
<dd>
<p>Nadat u Lenny toegevoegd heeft aan uw sources.list voor de opwaardering,
zult u hoogstwaarschijnlijk een waarschuwing zien over sleutel
4D270D06F42584E6 die ontbreekt. Dit is omdat het bestand Release voor Lenny
ondertekend werd met twee sleutels, één die beschikbaar is in Etch
en een andere die

<ifneq <current_release_etch> 4.0r7 "

er oorspronkelijk niet mee meegeleverd werd. Deze waarschuwing kan geen
kwaad, omdat beschikken over één sleutel om het bestand Release te verifiëren
voldoende is, maar uw Etch-systeem gewoon nog eenmaal bijwerken voordat u
opwaardeert naar Lenny zou de waarschuwing moeten doen verdwijnen
(versie 2009.01.31 van het pakket
<tt>debian-archive-keyring</tt> zou geïnstalleerd moeten worden).</p>

" "

dat niet is.</p>

<p>De waarschuwing kan geen kwaad, omdat de sleutel die beschikbaar is in Etch,
volstaat om het bestand Release te verifiëren, en de waarschuwing zal weggaan
nadat u opgewaardeert hebt naar Lenny. Indien u hierover echter bezorgd bent of
graag verlost zou worden van deze waarschuwing omdat Lenny wel een poos in uw
sources.list zal aanwezig blijven, zou u de versie van uw pakket
<tt>debian-archive-keyring</tt> moeten opwaarderen naar 2009.01.31 of later.
U kunt dit op de volgende plaatsen verkrijgen:</p>

<ul>
<li>rechtstreeks uit <a href="https://packages.debian.org/lenny/debian-archive-keyring">packages.debian.org</a></li>
<li>uit de pakketbron <em>etch-proposed-updates</em> door het volgende toe te voegen aan uw sources.list:
<pre>
  deb http://archive.debian.org/debian etch-proposed-updates main
</pre>
</li>
<li>uit de pakketbron volatile (u zult dit verkiezen als u volatile reeds in uw sources.list staan heeft)</li>
</ul>
<p>Er is een nieuwe tussenrelease van Etch voorzien met een bijgewerkt pakket
<tt>debian-archive-keyring</tt>.</p>

" />

</dd>
</dl>


<toc-add-entry name="security">Beveiligingsproblemen</toc-add-entry>

<p>Het Debian veiligheidsteam brengt updates uit voor pakketten uit de stabiele
release waarin problemen in verband met beveiliging vastgesteld werden.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina's</a> voor informatie
over eventuele beveiligingsproblemen die in <q>lenny</q> ontdekt werden.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan <tt>/etc/apt/sources.list</tt>
om toegang te hebben tot de laatste beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/ lenny/updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Tussenreleases</toc-add-entry>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Een
dergelijke bijwerking wordt gewoonlijk aangeduid als een tussenrelease.</p>

<ul>
  <li>De eerste tussenrelease, 5.0.1, werd uitgebracht op
      <a href="$(HOME)/News/2009/20090411">11 april 2009</a>.</li>
  <li>De tweede tussenrelease, 5.0.2, werd uitgebracht op
      <a href="$(HOME)/News/2009/20090627">27 juni 2009</a>.</li>
  <li>De derde tussenrelease, 5.0.3, werd uitgebracht op
      <a href="$(HOME)/News/2009/20090905">5 september 2009</a>.</li>
  <li>De vierde tussenrelease, 5.0.4, was released on
      <a href="$(HOME)/News/2010/20100130">30 januari 2010</a>.</li>
  <li>De vijfde tussenrelease, 5.0.5, werd uitgebracht op
      <a href="$(HOME)/News/2010/20100626">26 juni 2010</a>.</li>
  <li>De zesde tussenrelease, 5.0.6, werd uitgebracht op
      <a href="$(HOME)/News/2010/20100904">4 september 2010</a>.</li>
  <li>De zevende tussenrelease, 5.0.7, werd uitgebracht op
      <a href="$(HOME)/News/2010/20101127">27 november 2010</a>.</li>
  <li>De achtste tussenrelease, 5.0.8, werd uitgebracht op
      <a href="$(HOME)/News/2011/20110122">22 januari 2011</a>.</li>
  <li>De negende tussenrelease, 5.0.9, werd uitgebracht op
      <a href="$(HOME)/News/2011/20111001">1 oktober 2011</a>.</li>
  <li>De tiende tussenrelease, 5.0.10, werd uitgebracht op
      <a href="$(HOME)/News/2012/20120310">10 maart 2012</a>.</li>
</ul>

<ifeq <current_release_lenny> 5.0.0 "

<p>Er zijn nog geen tussenreleases voor Debian 5.0.</p>" "

<p>Zie de <a
href=http://archive.debian.org/debian/dists/lenny/ChangeLog>\
ChangeLog</a>
voor details over wijzigingen tussen 5.0.0 en <current_release_lenny/>.</p>"/>

<p>Verbeteringen voor de uitgebrachte stabiele distributie gaan dikwijls door een
uitgebreide testperiode voordat ze in het archief worden aanvaard.
Deze verbeteringen zijn echter wel beschikbaar in de map
<a href="http://archive.debian.org/debian/dists/lenny-proposed-updates/">\
dists/lenny-proposed-updates</a> van elke Debian archief-spiegelserver.</p>

<p>Als u APT gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# voorgestelde toevoegingen aan een tussenrelease van 5.0
  deb http://archive.debian.org/debian lenny-proposed-updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Installatiesysteem</toc-add-entry>

<p>
Raadpleeg voor informatie over errata en updates van het installatiesysteem
de pagina met installatie-informatie.
</p>
