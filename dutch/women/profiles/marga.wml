#use wml::debian::translation-check translation="08da87bf88f0132c810bd37c7312b5d0c4675a51"
<define-tag pagetitle>Margarita Manterola</define-tag>
#use wml::debian::profiles
#include "$(ENGLISHDIR)/women/profiles/profiles.def"

<profile name="Margarita Manterola" picture="marga.jpg">
    <URL>https://www.marga.com.ar/blog</URL>
    <email>marga@debian.org</email>

    <question1>
    <answer><p>
    Ik gebruik Debian sinds 2000. In het begin was ik gewoon een eenvoudige
    gebruiker, maar na verloop van tijd raakte ik meer betrokken, eerst door het
    rapporteren van bugs, toen door het indienen van patches, en tegen 2004 begon
    ik met het onderhouden van pakketten.
    <br /><br />
    Een keerpunt in mijn leven was
    <a href="https://wiki.debian.org/DebConf4">DebConf4</a> in Brazilië,
    waar ik veel Debian-ontwikkelaars heb ontmoet, gezichten op namen heb kunnen
    plakken en veel heb geleerd over hoe Debian werkt. Ik raad mensen ten zeerste
    aan om DebConf bij te wonen en daar mensen te ontmoeten. Het is al lang geleden
    en ik heb nu het gevoel dat Debian-mensen deel uitmaken van mijn uitgebreide
    familie.
    </p></answer>

    <question2>
    <answer><p>
    Ja. Ik ben Debian-ontwikkelaar geworden op 13 november 2005.
    </p></answer>

    <question3>
    <answer><p>
    Ik onderhoud een aantal pakketten, maar niet al te veel. Het belangrijkste op
    het gebied van verpakkingen dat ik doe, is deelnemen aan het team dat de
    Cinnamon Desktopomgeving onderhoudt.
    <br /><br />
    Ik ben ook zeer actief betrokken geweest bij de organisatie van verschillende
    DebConfs, in het bijzonder van
    <a href="https://wiki.debian.org/DebConf8">DebConf8</a>,
    dat plaatsvond in mijn land, Argentinië.
    Later verhuisde ik naar Duitsland en nam ook actief deel aan de organisatie van
    DebConf15, in Heidelberg. Bovendien vind ik het erg leuk om QA te doen: ik heb
    heel wat NMU's gedaan om RC-bugs op te lossen in pakketten die niet geschikt
    waren voor een release, en ik heb meestal veel plezier bij het deelnemen aan
    Bug Squashing Parties. Ik maak deel uit van het team tegen grensoverschrijdend
    gedrag, dat van Debian een veilige plek probeert te maken waar iedereen welkom
    is en zich kan uiten. Ik maak ook deel uit van het Technisch Comité, het orgaan
    dat helpt bij het nemen van moeilijke technische beslissingen.
    </p></answer>

    <question4>
    <answer><p>
    De filosofie van vrije software. Ik hield ervan dat die werd ontwikkeld door
    een gemeenschap in plaats van door een bedrijf en dat mijn bijdragen konden
    worden geaccepteerd als ze waardevol waren.<br /><br /> Ook de magie van apt-get
    en de immense pakketbron. Ik heb meestal het idee dat als iets niet in Debian
    zit, het niet de moeite waard is om het te gebruiken (en als dat wel het geval
    is, dan kan ik het op mij nemen om ervoor te zorgen dat het verpakt en geüpload
    wordt). Wat me door de jaren heen geïnteresseerd heeft gehouden in het werken
    in Debian is dat er altijd meer werk te doen is, meer software om in Debian te
    krijgen, meer bugs om op te lossen, meer nieuwe ideeën om uit te proberen.
    </p></answer>

    <question5>
    <answer><p>
    Er zijn veel dingen te doen in Debian, en meestal is het moeilijkste om uit te
    zoeken waar u best past. Als u bijvoorbeeld van programmeren houdt, zoek dan
    een team dat pakketten onderhoudt in een programmeertaal die u leuk vindt en
    sluit u bij hen aan. Als u bent zoals ik en graag veel kleine bugs oplost, kijk
    dan naar de lijst met bugs en probeer er één op te lossen. Er zijn veel
    eenvoudige bugs en mensen zullen u dankbaar zijn dat u de tijd hebt genomen om
    ze op te lossen. Maar zelfs als u niet bezig bent met coderen of bugs oplossen,
    zijn er veel dingen die u kunt doen. We hebben een beter grafisch ontwerp
    nodig, we hebben betere documentatie nodig, we hebben vertalingen nodig en nog
    veel meer.
    </p></answer>

    <question7>
    # Iets meer over uzelf...
    <answer><p>
    Ik ben al meer dan 10 jaar voornamelijk Python-programmeur. Tot 2012 woonde ik
    in Argentinië, daarna verhuisde ik naar München, Duitsland om voor Google te
    werken als verantwoordelijke voor de efficiënte werking van computer- en
    softwaresystemen.
    <br /><br />
    Ik ben sinds 2004 getrouwd met Maximiliano Curia (ook een DD) (DebConf4 was
    onze huwelijksreis!)
    </p></answer>
</profile>

