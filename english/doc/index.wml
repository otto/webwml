#use wml::debian::template title="Documentation" MAINPAGE="true"
#use wml::debian::recent_list

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">Quick Start</a></li>
  <li><a href="#manuals">Manuals</a></li>
  <li><a href="#other">Other (shorter) Documents</a></li>
</ul>

<p>
Creating a high-quality free operating system also includes writing technical manuals which describe the operation and use of programs. The Debian project is making every effort to provide all users with good documentation in an easily accessible form. This page contains a collection of links, leading to installation guides, HOWTOs, FAQs, release notes, our Wiki, and more.
</p>


<h2><a id="quick_start">Quick Start</a></h2>

<p>
If you are new to Debian we recommend you start with the following two guides:
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">Installation Guide</a></li>
  <li><a href="manuals/debian-faq/">Debian GNU/Linux FAQ</a></li>
</ul>

<p>
Do have these at hand when you make your first Debian installation, it
will probably answer many questions and help you work with your new Debian
system. 
</p>

<p>
Later, you might want to go through these documents:
</p>

<ul>
  <li><a href="manuals/debian-reference/">Debian Reference</a>: a terse user guide, focussing on shell commands</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Release Notes</a>: usually published with Debian updates, aiming at users upgrading the distribution</li>
  <li><a href="https://wiki.debian.org/">Debian Wiki</a>: official Debian wiki</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">Print the Debian GNU/Linux Reference Card</a></button></p>

<h2><a id="manuals">Manuals</a></h2>

<p>
Most of the documentation included in Debian was written for GNU/Linux in general, but there is also some documentation written specifically for Debian. Basically, the documents are sorted into these categories:
</p>

<ul>
  <li>Manuals: Those guides resemble books, because they comprehensively describe major topics. A lot of manuals listed below are available both online and in Debian packages. In fact, most of the manuals on the website are extracted from their respective Debian packages. Choose a manual below for its package name and/or links to the online versions.</li>
  <li>HOWTOs: Like the name indicates, the <a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">HOWTO documents</a> describe <em>how to</em> do a particular thing, i.e. they offer detailed and practical advice about the way to do something.</li>
  <li>FAQs: We have compiled several documents answering <em>frequently asked questions</em>. Debian-related questions are answered in the <a href="manuals/debian-faq/">Debian FAQ</a>. Additionally, there is a separate <a href="../CD/faq/">FAQ about Debian USB/CD/DVD images</a>, answering all sorts of questions about installation media.</li> 
  <li>Other (shorter) Documents:  Please also check the <a href="#other">list</a> of shorter instructions.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> For a complete list of Debian manuals and other documentation, please visit the <a href="ddp">Debian Documentation Project</a> page. Additionally, several user-oriented manuals written for Debian GNU/Linux are available as <a href="books">printed books</a>.</p>
</aside>

<p>Many of the manuals listed here are available both online and in Debian
packages; in fact, most of the manuals on the website are extracted from
their respective Debian packages. Choose a manual below for its package
name and/or links to the online versions.</p>

<h3>Manuals specific to Debian</h3>

<div class="line">
  <div class="item col50">
    
    <h4><a href="user-manuals">Manuals for Users</a></h4>
    <ul>
      <li><a href="https://debian-beginners-handbook.tuxfamily.org/index-en.html">The Debian Bookworm beginner’s handbook</a></li>
      <li><a href="user-manuals#faq">Debian GNU/Linux FAQ</a></li>
      <li><a href="user-manuals#install">Debian Installation Guide</a></li>
      <li><a href="user-manuals#relnotes">Debian Release Notes</a></li>
      <li><a href="user-manuals#refcard">Debian Reference Card</a></li>
      <li><a href="user-manuals#debian-handbook">The Debian Administrator's Handbook</a></li>
      <li><a href="user-manuals#quick-reference">Debian Reference</a></li>
      <li><a href="user-manuals#securing">Securing Debian Manual</a></li>
      <li><a href="user-manuals#aptitude">aptitude user's manual</a></li>
      <li><a href="user-manuals#apt-guide">APT User's Guide</a></li>
      <li><a href="user-manuals#java-faq">Debian GNU/Linux and Java FAQ</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Debian Hamradio Maintainer’s Guide</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Manuals for Developers</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Debian Policy Manual</a></li>
      <li><a href="devel-manuals#devref">Debian Developer's Reference</a></li>
      <li><a href="devel-manuals#debmake-doc">Guide for Debian Maintainers</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Introduction to Debian packaging</a></li>
      <li><a href="devel-manuals#menu">Debian Menu System</a></li>
      <li><a href="devel-manuals#d-i-internals">Debian Installer internals</a></li>
      <li><a href="devel-manuals#dbconfig-common">Guide for database using package maintainers</a></li>
      <li><a href="devel-manuals#dbapp-policy">Policy for packages using databases</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">Read the Debian Project History</a></button></p>

<h2><a id="other">Other (shorter) Documents</a></h2>

<p>The following documents include quicker, shorter instructions:</p>

<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> If you have checked the above resources and still can't find answers to your questions or solutions to your problems regarding Debian, take a look at our <a href="../support">support page</a>.</p>
</aside>

  <dt><strong>Manpages</strong></dt>
    <dd>Traditionally, all Unix programs are documented with
    <em>manpages</em>,
        reference manuals made available through the <tt>man</tt>
        command. They usually aren't meant for beginners, but
        document all features and functions of a command. The complete
        repository of all manpages available in Debian is online: <a
        href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">info files</a></strong></dt>
    <dd>Many GNU software is documented through <em>info 
        files</em> instead of manpages. These files include detailed
	information of the program itself, options and example usage.
	Info pages are available through the <tt>info</tt>
        command.
    </dd>

  <dt><strong>README files</strong></dt>
    <dd>README files are simple text
        files describing a single item, usually a package. You can find a
        lot of these files in the <tt>/usr/share/doc/</tt> subdirectories
        on your Debian system. In addition to the README file, some of
        those directories include configuration examples. Please note
        that larger programs' documentation is typically provided in a
        separate package (same name as the original package, but ending
        in <em>-doc</em>).
    </dd>
</dl>
</p>
