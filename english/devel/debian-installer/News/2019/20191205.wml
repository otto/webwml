<define-tag pagetitle>Debian Installer Bullseye Alpha 1 release</define-tag>
<define-tag release_date>2019-12-05</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the first alpha release of the installer for Debian 11
<q>Bullseye</q>.
</p>

<p>
It's high time we started doing this: many components were updated,
replacing “CD”/“CD-ROM” with “installation media”. Such changes are
not documented individually below. That also explains why many
languages are not fully translated in this alpha release.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>apt-setup:
    <ul>
      <li>Update generation of apt sources lines for security: renamed
        from <em>dist</em>/updates to <em>dist</em>-security starting with bullseye (<a href="https://bugs.debian.org/935540">#935540</a>,
        <a href="https://bugs.debian.org/942238">#942238</a>).</li>
      <li>Fix preseeding for local repositories by adding the key(s)
        in the /etc/apt/trusted.gpg.d directory (<a href="https://bugs.debian.org/851774">#851774</a>, <a href="https://bugs.debian.org/928931">#928931</a>).</li>
    </ul>
  </li>
  <li>base-installer:
    <ul>
      <li>Stop installing the apt-transport-https transitional package.</li>
    </ul>
  </li>
  <li>brltty:
    <ul>
      <li>Stop creating my-at-spi-dbus-bus.desktop in user profile,
        at-spi2-core now always start the at-spi bus.</li>
    </ul>
  </li>
  <li>choose-mirror:
    <ul>
      <li>Update Mirrors.masterlist.</li>
      <li>Use "deb.debian.org" as default hostname for custom HTTP
        mirror.</li>
    </ul>
  </li>
  <li>console-setup:
    <ul>
      <li>Fix internationalization issues (<a href="https://bugs.debian.org/924657">#924657</a>).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Target Bullseye!</li>
      <li>Allow to have several spaces around [] qualifiers in
        sources.list entries.</li>
      <li>Restore 'd' shortcut for the dark theme. 'c' cannot work in
        grub (<a href="https://bugs.debian.org/935545">#935545</a>).</li>
      <li>Set gfxpayload=keep in submenus too, to fix unreadable fonts
        on HiDPI displays in netboot images booted with EFI
        (<a href="https://bugs.debian.org/935546">#935546</a>).</li>
      <li>Convert some documentation to DocBook XML 4.5 (<a href="https://bugs.debian.org/907970">#907970</a>).</li>
    </ul>
  </li>
  <li>finish-install:
    <ul>
      <li>Stop creating my-at-spi-dbus-bus.desktop in user profile,
        at-spi2-core now always start the at-spi bus.</li>
    </ul>
  </li>
  <li>fonts-sil-abyssinica:
    <ul>
      <li>Do not strip the font of Latin characters for the
        installer.</li>
    </ul>
  </li>
  <li>grub2:
    <ul>
      <li>Add probe module to signed UEFI images (<a href="https://bugs.debian.org/936082">#936082</a>).</li>
    </ul>
  </li>
  <li>netcfg:
    <ul>
      <li>Reword template since some DHCP clients are no longer
        available.</li>
    </ul>
  </li>
  <li>parted:
    <ul>
      <li>Cherry-pick upstream patch to remove output to stdout from
        affs that confused the installer (<a href="https://bugs.debian.org/941777">#941777</a>).</li>
    </ul>
  </li>
  <li>partman-crypto:
    <ul>
      <li>Install cryptsetup-initramfs instead of cryptsetup
        (<a href="https://bugs.debian.org/930228">#930228</a>).</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-installer:
    <ul>
      <li>Remove images for QNAP TS-11x/TS-21x/HS-21x, QNAP
        TS-41x/TS-42x and HP Media Vault mv2120 due to size problems
        with the Linux kernel.</li>
      <li>ARM: sunxi: Add support for Olimex
        A20-OLinuXino-Lime2-eMMC.</li>
      <li>Tweak mini.iso generation on arm so EFI netboot will
        work.</li>
    </ul>
  </li>
  <li>hw-detect:
    <ul>
      <li>Install virtualization related packages when virtualization
        is detected (<a href="https://bugs.debian.org/782287">#782287</a>).</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>[arm64] udeb: Add i2c-rk3x to i2c-modules.</li>
      <li>[arm64,armhf] udeb: Add rockchip-io-domain to
        kernel-image.</li>
      <li>udeb: Add atmel_mxt_ts to input-modules.</li>
      <li>udeb: input-modules: Add OLPC AP-SP keyboard.</li>
      <li>[arm64] udeb: Add pl330 to kernel-image.</li>
      <li>[armhf] udeb: Remove davinci_cpdma from nic-modules.</li>
      <li>udeb: Include physmap instead of physmap_of in
        mtd-modules.</li>
      <li>udeb: Add thermal_sys to kernel-image.</li>
      <li>udeb: Add virtio-gpu to get graphical output in VM
        instances.</li>
      <li>[x86] udeb: Move rfkill to new rfkill-modules package to
        avoid duplication.</li>
      <li>[arm] Backport DTB support for Rasperry Pi Compute Module
        3.</li>
      <li>[arm64] Backport DTB support for Rasperry Pi Compute Module
        3.</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>76 languages are supported in this release.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
