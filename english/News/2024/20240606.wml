# Status: published
# $Id$
# $Rev$

<define-tag pagetitle>Statement on Daniel Pocock</define-tag>
<define-tag release_date>2024-06-06</define-tag>
#use wml::debian::news

<p>Following the undue registration of the Debian trademark in Switzerland
by the company of an ex-Debian Developer - who has been conducting a heavy
smear campaign against Debian since 2018 -, the Debian project has taken
robust action to secure its trademarks and interests world-wide.</p>

<p>In November 2023, following a legal action brought by Debian, the Tribunal
Cantonal of the Canton of Vaud <a
href="https://www.debian.org/News/2024/judgement.pdf">ruled</a> that the
registration of the DEBIAN mark in Switzerland by Open Source Developer
Freedoms SA in liquidation (OSDF) – formerly Software Freedom Institute SA –
was a misappropriation of the Debian trademark, as the latter is well known
and established in the IT sector worldwide. No appeal was lodged against
the judgment which is therefore final.</p>

<p>In its judgement the tribunal found that:</p>

<p><em>"Daniel Pocock is the sole administrator of the defendant [OSDF]."</em></p>

<p><em>"By proceeding in this way, the defendant, who was aware of the existence
of the disputed trade mark and its reputation, and who knew that the plaintiff
was its owner, since its sole administrator was a Debian developer and a
former member of the community of the same name, usurped the said trade mark
and created confusion in the mind of the public."</em></p>

<p>The tribunal ordered that the Swiss trademark registration be transferred
to the Debian project's trusted organisation, Software in the Public
Interest (SPI) Inc., and the costs of the action paid by the defendant. OSDF
was also ordered to publish the ruling on its website. Before the court's
final ruling could be handed down, OSDF abruptly cancelled the registration
and later entered into liquidation without notifying SPI and without paying
the costs of the action. To date, Daniel Pocock has failed to comply with
any of the court's orders and Debian has been forced to commence recovery
action for its costs.</p>

<p>Meanwhile, between 2020 and February 2024, Debian became aware of at
least 14 domain registrations and associated websites contravening our 
trademark policy. All the domains were registered and controlled by Daniel
Pocock. In May 2024 the World Intellectual Property Organisation (WIPO)
directed the relevant domain registries to
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2024-0770">\
transfer</a> all 14 registrations to SPI Inc. in trust for Debian,
<a href="https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770">\
noting</a>:</p>

<p><em>"[...] the Respondent registered and used the disputed domain names in bad
faith, creating a likelihood of confusion as to source or affiliation with
the Complainant’s trademark, for commercial as well as critical purposes,
and with full knowledge of the Complainant’s trademark and trademark policy."</em></p>

<p>The Debian Project will continue to take all necessary measures to protect
and defend its trademarks and other interests. Our trademark policy has stood
up to scrutiny in multiple jurisdictions.</p>

<p>We thus take this opportunity to thank the members of our community who
invested significant effort documenting Debian's use and deployment around
the world in preparation to challenge the Swiss trademark registration,
despite the heavy – and unfair – smear campaign many of our volunteers had
to face in the past years.</p>

<p>We remain committed to pursuing appropriate legal means to protect our
community and others from further harassment.</p>

<h2>Further information</h2>

<p>The judgment of the Tribunal Cantonal of the Canton of Vaud, dated
November 27, 2023, can be found is at <url "https://www.debian.org/News/2024/judgement.pdf">
and the WIPO full decision text, dated May 3, 2024, is available at <a
href="https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770">\
https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770</a></p>


<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>

