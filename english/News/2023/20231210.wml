<define-tag pagetitle>Updated Debian 12: 12.4 released</define-tag>
<define-tag release_date>2023-12-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Please be advised that this document has been updated as best to reflect
Debian 12.3 being superseded by Debian 12.4. These changes came about from a
last minute bug advisory of <a href=https://bugs.debian.org/1057843>#1057843</a>
concerning issues with linux-image-6.1.0-14 (6.1.64-1).</p>

<p>Debian 12.4 is released with linux-image-6.1.0-15 (6.1.66-1), along with a few
other bug fixes</p>

<p>The Debian project is pleased to announce the fourth update of its
stable distribution Debian <release> (codename <q><codename></q>).
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction adequate "Skip symbol-size-mismatch test on architectures where array symbols don't include a specific length; disable deprecation warnings about smartmatch, given, when in Perl 5.38; fix warnings from version comparison about smartmatch being experimental">
<correction amanda "Fix local privilege escalation [CVE-2023-30577]">
<correction arctica-greeter "Move logo away from border when greeting">
<correction awstats "Avoid prompts on upgrade due to logrotate configuration cleanup">
<correction axis "Filter out unsupported protocols in the client class ServiceFactory [CVE-2023-40743]">
<correction base-files "Update for the 12.4 point release">
<correction ca-certificates-java "Remove circular dependencies">
<correction calibre "Fix crash in Get Books when regenerating UIC files">
<correction crun "Fix containers with systemd as their init system, when using newer kernel versions">
<correction cups "Take into account that on some printers the ColorModel option's choice for color printing is CMYK and not RGB">
<correction dav4tbsync "New upstream version, restoring compatibility with newer Thunderbird versions">
<correction debian-edu-artwork "Provide an Emerald theme based artwork for Debian Edu 12">
<correction debian-edu-config "New upstream stable version; fix setting and changing of LDAP passwords">
<correction debian-edu-doc "Update included documentation and translations">
<correction debian-edu-fai "New upstream stable version">
<correction debian-edu-router "Fix dnsmasq conf generation for networks over VLAN; only generate UIF filter rules for SSH if 'Uplink' interface is defined; update translations">
<correction debian-installer "Increase Linux kernel ABI to 6.1.0-15; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debootstrap "Backport merged-/usr support changes from trixie: implement merged-/usr by post-merging, default to merged-/usr for suites newer than bookworm in all profiles">
<correction devscripts "Debchange: Update to current Debian distributions">
<correction dhcpcd5 "Change Breaks/Replaces dhcpcd5 to Conflicts">
<correction di-netboot-assistant "Fix support for bookworm live ISO image">
<correction distro-info "Update tests for distro-info-data 0.58+deb12u1, which adjusted Debian 7's EoL date">
<correction distro-info-data "Add Ubuntu 24.04 LTS Noble Numbat; fix several End Of Life dates">
<correction eas4tbsync "New upstream version, restoring compatibility with newer Thunderbird versions">
<correction exfatprogs "Fix out-of-bounds memory access issues [CVE-2023-45897]">
<correction exim4 "Fix security issues relating to the proxy protocol [CVE-2023-42117] and DNSDB lookups [CVE-2023-42119]; add hardening for SPF lookups; disallow UTF-16 surrogates from ${utf8clean:...}; fix crash with <q>tls_dhparam = none</q>; fix $recipients expansion when used within ${run...}; fix expiry date of auto-generated SSL certificates; fix crash induced by some combinations of zero-length strings and ${tr...}">
<correction fonts-noto-color-emoji "Add support for Unicode 15.1">
<correction gimp "Add Conflicts and Replaces: gimp-dds to remove old versions of this plugin shipped by gimp itself since 2.10.10">
<correction gnome-characters "Add support for Unicode 15.1">
<correction gnome-session "Open text files in gnome-text-editor if gedit is not installed">
<correction gnome-shell "New upstream stable release; allow notifications to be dismissed with backspace key in addition to the delete key; fix duplicate devices shown when reconnecting to PulseAudio; fix possible use-after-free crashes on PulseAudio/Pipewire restart; avoid sliders in quick settings (volume, etc.) being reported to accessibility tools as their own parent object; align scrolled viewports to the pixel grid to avoid jitter visible during scrolling">
<correction gnutls28 "Fix timing sidechannel issue [CVE-2023-5981]">
<correction gosa "New upstream stable release">
<correction gosa-plugins-sudo "Fix uninitialised variable">
<correction hash-slinger "Fix generation of TLSA records">
<correction intel-graphics-compiler "Fix compatibility with stable's intel-vc-intrinsics version">
<correction iotop-c "Fix the logic in <q>only</q> option; fix busy loop when ESC is pressed; fix ASCII graph rendering">
<correction jdupes "Update prompts to help avoid choices that could lead to unexpected data loss">
<correction lastpass-cli "New upstream stable release; update certificate hashes; add support for reading encrypted URLs">
<correction libapache2-mod-python "Ensure binNMU versions are PEP-440-compliant">
<correction libde265 "Fix segmentation violation issue [CVE-2023-27102], buffer overflow issues [CVE-2023-27103 CVE-2023-47471], buffer over-read issue [CVE-2023-43887]">
<correction libervia-backend "Fix start failure without pre-existing configuration; make exec path absolute in dbus service file; fix dependencies on python3-txdbus/python3-dbus">
<correction libmateweather "Locations: add San Miguel de Tucuman (Argentina); update forecast zones for Chicago; update data server URL; fix some location names">
<correction libsolv "Enable support for zstd compression">
<correction linux "Update to upstream stable release 6.1.66; update ABI to 15; [rt] Update to 6.1.59-rt16; enable X86_PLATFORM_DRIVERS_HP; nvmet: nul-terminate the NQNs passed in the connect command [CVE-2023-6121]">
<correction linux-signed-amd64 "Update to upstream stable release 6.1.66; update ABI to 15; [rt] Update to 6.1.59-rt16; enable X86_PLATFORM_DRIVERS_HP; nvmet: nul-terminate the NQNs passed in the connect command [CVE-2023-6121]">
<correction linux-signed-arm64 "Update to upstream stable release 6.1.66; update ABI to 15; [rt] Update to 6.1.59-rt16; enable X86_PLATFORM_DRIVERS_HP; nvmet: nul-terminate the NQNs passed in the connect command [CVE-2023-6121]">
<correction linux-signed-i386 "Update to upstream stable release 6.1.66; update ABI to 15; [rt] Update to 6.1.59-rt16; enable X86_PLATFORM_DRIVERS_HP; nvmet: nul-terminate the NQNs passed in the connect command [CVE-2023-6121]">
<correction llvm-toolchain-16 "New backported package to support builds of newer chromium versions">
<correction lxc "Fix creating of ephemeral copies">
<correction mda-lv2 "Fix LV2 plugin installation location">
<correction midge "Remove non-free example files">
<correction minizip "Fix integer and heap overflow issues [CVE-2023-45853]">
<correction mrtg "Handle relocated configuration file; translation updates">
<correction mutter "New upstream stable release; fix the ability to drag libdecor windows by their title bar on touchscreens; fix flickering and rendering artifacts when using software rendering; improve GNOME Shell app grid performance by avoiding repainting monitors other than the one it is displayed on">
<correction nagios-plugins-contrib "Fix on-disk kernel version detection">
<correction network-manager-openconnect "Add User Agent to Openconnect VPN for NetworkManager">
<correction node-undici "Delete cookie and host headers on cross-origin redirect [CVE-2023-45143]">
<correction nvidia-graphics-drivers "New upstream release; fix null pointer dereference issue [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla "New upstream release; fix null pointer dereference issue [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "New upstream release; fix null pointer dereference issue [CVE-2023-31022]">
<correction nvidia-open-gpu-kernel-modules "New upstream release; fix null pointer dereference issue [CVE-2023-31022]">
<correction opendkim "Fix removal of incoming Authentication-Results: headers [CVE-2022-48521]">
<correction openrefine "Fix remote code execution vulnerability [CVE-2023-41887 CVE-2023-41886]">
<correction opensc "Fix out-of-bounds read issue [CVE-2023-4535], potential PIN bypass [CVE-2023-40660], memory-handling issues [CVE-2023-40661]">
<correction oscrypto "Fix OpenSSL version parsing; fix autopkgtest">
<correction pcs "Fix <q>resource move</q>">
<correction perl "Fix buffer overrun issue [CVE-2023-47038]">
<correction php-phpseclib3 "Fix denial of service issue [CVE-2023-49316]">
<correction postgresql-15 "New upstream stable release; fix SQL injection issue [CVE-2023-39417]; fix MERGE to enforce row security policies properly [CVE-2023-39418]">
<correction proftpd-dfsg "Fix size of SSH key exchange buffers">
<correction python-cogent "Only skip tests that require multiple CPUs when running on a single CPU system">
<correction python3-onelogin-saml2 "Fix expired test payloads">
<correction pyzoltan "Support building on single core systems">
<correction qbittorrent "Disable UPnP for web UI by default in qbittorrent-nox">
<correction qemu "Update to upstream stable release 7.2.7; hw/scsi/scsi-disk: Disallow block sizes smaller than 512 [CVE-2023-42467]">
<correction qpdf "Fix data loss issue with some quoted octal strings">
<correction redis "Drop ProcSubset=pid hardening flag from the systemd unit due to it causing crashes">
<correction rust-sd "Ensure binary package versions sorts correctly relative to older releases (where it was built from a different source package)">
<correction sitesummary "Use systemd timer for running sitesummary-client if available">
<correction speech-dispatcher-contrib "Enable voxin on armhf and arm64">
<correction spyder "Fix interface language auto-configuration">
<correction symfony "Fix session fixation issue [CVE-2023-46733]; add missing escaping [CVE-2023-46734]">
<correction systemd "New upstream stable release">
<correction tbsync "New upstream version, restoring compatibility with newer Thunderbird versions">
<correction toil "Only request a single core for tests">
<correction tzdata "Update leap second list">
<correction unadf "Fix buffer overflow issue [CVE-2016-1243]; fix code execution issue [CVE-2016-1244]">
<correction vips "Fix null pointer dereference issue [CVE-2023-40032]">
<correction weborf "Fix denial of service issue">
<correction wormhole-william "Disable flaky tests, fixing build failures">
<correction xen "New upstream stable update; fix several security issues [CVE-2022-40982 CVE-2023-20569 CVE-2023-20588 CVE-2023-20593 CVE-2023-34320 CVE-2023-34321 CVE-2023-34322 CVE-2023-34323 CVE-2023-34325 CVE-2023-34326 CVE-2023-34327 CVE-2023-34328 CVE-2023-46835 CVE-2023-46836]">
<correction yuzu "Strip :native from glslang-tools build dependency, fixing build failure">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5521 tomcat10>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5525 samba>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5529 slurm-wlm-contrib>
<dsa 2023 5529 slurm-wlm>
<dsa 2023 5531 roundcube>
<dsa 2023 5532 openssl>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5541 request-tracker5>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 jtreg6>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5552 ffmpeg>
<dsa 2023 5553 postgresql-15>
<dsa 2023 5555 openvpn>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5559 wireshark>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5562 tor>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5568 fastdds>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
</table>


<h2>Removed packages</h2>

<p>The following packages were removed due to circumstances beyond our control:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction gimp-dds "No longer required; integrated into GIMP">

</table>

<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


