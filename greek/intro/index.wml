#use wml::debian::template title="Εισαγωγή στο Debian" MAINPAGE="true" FOOTERMAP="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="galaxico"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Το Debian είναι μια Κοινότητα</h2>
      <p>Χιλιάδες εθελοντές σε ολόκληρο τον κόσμο δουλεύουν από κοινού βάζοντας ως προτεραιότητα το Ελεύθερο και το Ανοιχτό Λογισμικό. Γνωρίστε το Σχέδιο Debian.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Οι Άνθρωποι:</a>
          Ποιοι/ες είμαστε και τι κάνουμε
        </li>
        <li>
          <a href="philosophy">Η Φιλοσοφία μας:</a>
          Γιατί το κάνουμε και πώς το κάνουμε
        </li>
        <li>
          <a href="../devel/join/">Εμπλακείτε:</a>
          Πώς μπορείτε να συμβάλλετε στο Debian
        </li>
        <li>
          <a href="help">Συνεισφέρετε:</a>
          Πώς μπορείτε να βοηθήσετε το Debian
        </li>
        <li>
          <a href="../social_contract">Το Κοινωνικό Συμβόλαιο του Debian:</a>
          Η ηθική μας ατζέντα
        </li>
        <li>
          <a href="diversity">Όλοι/ες είναι καλοδεχούμενοι/ες:</a>
          Η Δήλωση του Debian για τη Διαφορετικότητα
        </li>
        <li>
          <a href="../code_of_conduct">Για όσους/ες συμμετέχουν:</a>
          Ο Κώδικας συμπεριφοράς του Debian
        </li>
        <li>
          <a href="../partners/">Συνεργάτες:</a>
          Εταιρείες και οργανισμοί που βοηθούν το Σχέδιο Debian
        </li>
        <li>
          <a href="../donations">Δωρεές:</a>
          Πώς μπορείτε να χορηγήσετε το Σχέδιο Debian
        </li>
        <li>
          <a href="../legal/">Νομικά Ζητήματα:</a>
          Άδειες χρήσης, εμπορικά σήματα, πολιτική ιδιωτικότητας, πολιτική πατεντών, κλπ.
        </li>
        <li>
          <a href="../contact">Επικοινωνία:</a>
          Πώς μπορείτε να έρθετε σε επαφή μαζί μας
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Το Debian είναι ένα Λειτουργικό Σύστημα</h2>
      <p>Το Debian είαι ένα ελεύθερο λειτουργικό σύστημα, που αναπτύσσεται και συντηρείται από το Σχέδιο Debian. Μια ελεύθερη διανομή Linux με χιλιάδες εφαρμογές για την κάλυψη των αναγκών των χρηστών μας.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Μεταφόρτωση:</a>
          Από πού μπορείτε να αποκτήσετε το Debian
        </li>
        <li>
          <a href="why_debian">Γιατί το Debian:</a>
          Λόγοι για να διαλέξετ το Debian
        </li>
        <li>
          <a href="../support">Υποστήριξη:</a>
          Πού μπορείτε να βρείτε βοήθεια
        </li>
        <li>
          <a href="../security">Ασφάλεια:</a>
          Πιο πρόσφατη επικαιροποίηση <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Λογισμικό:</a>
          Αναζητήστε και περιηγηθείτε στη μεγάλη λίστα των πακέτων του Debian
        </li>
        <li>
          <a href="../doc">Τεκμηρίωση:</a>
          Οδηγός εγκατάστασης, Συχνές ερωτήσεις (FAQ), HOWTOs, Wiki, και ακόμα περισσότερα
        </li>
        <li>
          <a href="../bugs">Σύστημα Παρακολούθησης Σφαλμάτων (BTS):</a>
          Πώς να αναφέρετε ένα σφάλμα, τεκμηρίωση για το BTS
        </li>
        <li>
          <a href="https://lists.debian.org/">Λίστες Αλληλογραφίας:</a>
          Συλλογή με τις λίστες αλληλογραφίας του Debian για χρήστες, προγραμματίστ(ρι)ες κλπ.
        </li>
        <li>
          <a href="../blends">Καθαρά Μείγματα:</a>
          Μεταπακέτα για συγκεκριμένες ανάγκες
        </li>
        <li>
          <a href="../devel">Η Γωνιά των Προγραμματιστ(ρι)ών:</a>
          Πληροφορίες που έχουν σημασία κυρίως για τους/τις προγραμματίστριες του Debian
        </li>
        <li>
          <a href="../ports">Υλοποιήσεις/Αρχιτεκτονικές:</a>
          Η υποστήριξη του Debian για διάφορες αρχιτεκτονικές επεξεργαστών
        </li>
        <li>
          <a href="search">Αναζήτηση:</a>
          Πληροφορίες για το πώς να χρησιμοποιήσετε τη μηχανή αναζήτησης του Debian
        </li>
        <li>
          <a href="cn">Γλώσσες:</a>
          Ρυθμίσεις της γλώσσας για τον ιστότοπο του Debian
        </li>
      </ul>
    </div>
  </div>

</div>

