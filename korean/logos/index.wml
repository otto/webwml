#use wml::debian::template title="데비안 로고" BARETITLE=true
#include "$(ENGLISHDIR)/logos/index.data"
#use wml::debian::translation-check translation="97732ca8593e39ce8b981adf7f81657417b62c73"

<p>데비안은 두 로고가 있습니다. <a href="#open-use">공식 로고</a>(<q>열린 사용 로고</q>로 알려짐)는 잘 알려진 데비안 <q>소용돌이</q>이며 데비안 프로젝트의 데비안 프로젝트의 시각적 정체성을 가장 잘 나타냅니다. 별도, <a
  href="#restricted-use">제한 사용 로고</a>는, 데비안 프로젝트와 그 멤버만 사용할 수 있습니다.
데비안을 참조하려면 열린 로고를 사용하세요.</p>

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="open-use">데비안 열린 사용 로고</a></th>
</tr>
<tr>
<td>

<p>데비안 열린 사용 로고는 두 가지, &ldquo;데비안&rdquo; 레이블 있는 것과 없는 것입니다.</p>

<p>데비안 열린 로고는 저작권 (c) 1999
  <a href="https://www.spi-inc.org/">Software in the Public Interest, Inc.</a> 이며,
  <a href="https://www.gnu.org/copyleft/lgpl.html">GNU Lesser General Public
  License</a>, 버전 3 또는 이후 버전, 또는, 여러분의 선택에 따라
  <a href="https://creativecommons.org/licenses/by-sa/3.0/">Creative Commons
  Attribution-ShareAlike 3.0 Unported License</a>입니다.</p>

<p>주의: 웹페이지에서 사용하면 이미지에 <a href="$(HOME)">https://www.debian.org/</a> 링크를 만들수 있습니다.</p>
</td>
<td>
<openlogotable>
</td>
</tr>
</table>

<hr />

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="restricted-use">데비안 제한 사용 로고</a></th>
</tr>
<tr>
<td>
<h3>데비안 제한 사용 로고 저작권</h3>

<p>Copyright (c) 1999 Software in the Public Interest</p>
<ol>
	<li>This logo may only be used if:
         <ul>
	    <li>the product it is used for is made using a documented
		procedure as published on www.debian.org (for example
		official CD-creation), or </li>
	    <li>official approval is given by Debian for its use in this
                purpose </li>
         </ul>
	 </li>
	<li>May be used if an official part of debian (decided using the rules
         in I) is part of the complete product, if it is made clear that only
         this part is officially approved </li> 
	<li>We reserve the right to revoke a license for a product</li>
</ol>

<p>Permission has been given to use the restricted logo on clothing (shirts,
hats, etc) as long as they are made by a Debian developer and not sold for
profit.</p>
</td>
<td>
<officiallogotable>
</td>
</tr>
</table>

<hr />

<h2>데비안 로고에 대하여</h2>
<p>데비안 로고는 1999년 데비안 개발자 투표로 선택되었습니다.
로고는 <a href="mailto:rsilva@debian.org">Raul Silva</a>가 만들었습니다.
글꼴에 사용된 빨간 색은 <strong>Rubine Red 2X CVC</strong>입니다. 최근
용어로 해당되는 색은 PANTONE Strong Red C (RGB로 #CE0056) 또는 PANTONE Rubine Red C
(RGB로 #CE0058) 색입니다. 로고에 대해 더 자세한 사항과, 왜 PANTONE Rubine Red 2X CVC 색이
바뀌었고 다른 빨간 색을 대신 쓰게 되었는지는
<a href="https://wiki.debian.org/DebianLogo">데비안 로고 위키 페이지</a>를 보세요.</p>

<h2>다른 프로모션 이미지</h2>

<h3>데비안 버튼</h3>

<p><img class="ico" src="button-1.gif" alt="[Debian GNU/Linux]" />
이는 프로젝트에 처음 사용되었던 버튼입니다. 이 로고의 라이선스는
Open Use logo license입니다. 이 버튼은
<a href="mailto:csmall@debian.org">Craig Small</a>이 만들었습니다.</p>

<p>다음은 데비안에 사용된 다른 버튼입니다:</p>
<br />
<morebuttons>

<h3>데비안 다양성 로고</h3>

<p>여러가지 데비안 로고가 커뮤니티의 다양성을 홍보하는데 사용됩니다.
데비안 다양성 로고라고 합니다:
<br/>
<img src="diversity-2019.png" alt="[Debian Diversity logo]" />
<br/>
이 로고는 <a href="https://wiki.debian.org/ValessioBrito">Valessio Brito</a>가 만들었습니다.
라이선스는 GPLv3 입니다.
소스는 (SVG 형식) 제작자의 <a href="https://gitlab.com/valessiobrito/artwork">Git 저장소</a>에
있습니다..
<br />
</p>

<h3>데비안 육각 스티커</h3>

<p>다음은
<a href="https://github.com/terinjokes/StickerConstructorSpec">육각 스티커</a>입니다:
<br/>
<img src="hexagonal.png" alt="[Debian GNU/Linux]" />
<br/>
소스 (SVG 형식) 및 PNG와 SVG 미리 보기를 만드는 Makefile은
<a href="https://salsa.debian.org/debian/debian-flyers/tree/master/hexagonal-sticker">Debian flyers 저장소</a>에
있습니다.

이 스티커의 라이선스는 Open Use logo license입니다.
이 스티커는 <a href="mailto:valhalla@trueelena.org">Elena Grandi</a>가 만들었습니다.</p>
<br />

#
# Logo font: Poppl Laudatio Condensed
#
# https://lists.debian.org/debian-www/2003/08/msg00261.html
#
