#use wml::debian::cdimage title="Scaricare le immagini per USB/CD/DVD Debian via HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="019351f156c8f8ffc0f7922929f7062fb4b732db" maintainer="Luca Monducci"

<p>Sono disponibili per il download le seguenti immagini Debian:</p>

<ul>

  <li><a href="#stable">Immagini ufficiali per USB/CD/DVD della distribuzione
  <q>stable</q></a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Immagini
  ufficiali per USB/CD/DVD della distribuzione <q>testing</q>(<em>generate
  settimanalmente</em>)</a></li>

</ul>

<p>Vedere anche:</p>

<ul>

  <li>L'elenco completo dei <a href="#mirrors">mirror di <tt>debian-cd/</tt></a></li>

  <li>Le immagini <q>netinst</q>,
  si veda la pagina dell'<a href="../netinst/">installazione
  via rete</a>.</li>

  <li>Le immagini della distribuzione <q>testing</q> nella pagina dell'<a
  href="$(DEVEL)/debian-installer/">Installatore Debian</a>.</li>

</ul>

<hr />

<h2><a name="stable">Immagini ufficiali per USB/CD/DVD della distribuzione <q>stable</q></a></h2>

<p>Per installare Debian su una macchina senza una connessione a Internet,
è possibile usare le immagini per CD/USB (700&nbsp;MB ciascuna) oppure le
immagini dei DVD/USB (4,4&nbsp;GB ciascuna). Scaricare il primo file immagine
per CD/USB o per DVD/USB, scrivere l'immagine su un disco con un masterizzatore
CD/DVD oppure su una chiavetta USB e poi avviare la macchina con quello.</p>

<p>La <strong>prima</strong> immagine per USB/CD/DVD contiene tutti i file
necessari all'installazione di un sistema Debian standard.</p>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>

<p>I seguenti collegamenti puntano ai file con le immagini di dimensione
fino a 650&nbsp;MB, e quindi adatti per essere scritti su un comune supporto
CD-R(W):</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>

<p>I seguenti collegamenti puntano ai file con le immagini di dimensione
fino a 4,4&nbsp;GB, e quindi adatti per essere incisi su un comune supporto
DVD-R/DVD+R:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Prima di procedere con l'installazione si dovrebbe consultare
la documentazione. <strong>Volendo leggere un solo documento</strong> per
l'installazione, leggere l'<a href="$(HOME)/releases/stable/amd64/apa">Installation
Howto</a>, una <q>passeggiata</q> su tutto il processo di installazione.
Altri documenti utili sono:</p>

<ul>

  <li>La <a href="$(HOME)/releases/stable/installmanual">Guida
  all'installazione</a>, con istruzioni dettagliate per l'installazione</li>

  <li>La <a href="https://wiki.debian.org/DebianInstaller">documentazione del
  Debian-Installer</a>, comprese le FAQ con le domande comuni e relative
  risposte</li>

  <li>L'<a href="$(HOME)/releases/stable/debian-installer/#errata">Errata del
  Debian-Installer</a>, l'elenco dei problemi conosciuti dell'installatore</li>

</ul>

<hr />

<h2><a name="mirrors">Mirror conosciuti dell'archivio <q>debian-cd</q></a></h2>

<p>N.B. <strong>alcuni mirror potrebbero non essere aggiornati</strong>,
l'attuale release <q>stabile</q> delle immagini per USB/CD/DVD è la
<strong><current-cd-release></strong>.</p>

<p><strong>In caso di dubbi usare il
<a href="https://cdimage.debian.org/debian-cd/">server primario delle
immagini dei CD</a> in Svezia</strong>.</p>

<p>Per pubblicare dal proprio mirror le immagini dei CD Debian si consultino
le <a href="../mirroring/">istruzioni su come realizzare un mirror per le
immagini dei CD</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
