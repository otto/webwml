#use wml::debian::translation-check translation="2a1830b31e7529340bf703cd96b2b22677f850df" maintainer="Giuseppe Sacco"
<define-tag pagetitle>Supporto della sicurezza per Bullseye passato al team LTS</define-tag>
<define-tag release_date>2024-08-14</define-tag>
#use wml::debian::news

<p>
A partire dal 14 agosto 2024, tre anni dopo il rilascio iniziale, il supporto
normale della sicurezza per Debian 11, alias <q>Bullseye</q>, è arrivato all
fine. Il team Debian per il <a href="https://wiki.debian.org/LTS/">supporto
a lungo termine (Long Term Support, LTS)</a> proseguirà il supporto della
sicurezza che precedentemente era in carico a ai team del rilascio e a quello
della sicurezza.
</p>

<h2>Informazioni per gli utenti</h2>

<p>
Bullseye LTS verrà supportata dal 15 agosto 2024 al 31 agosto 2026.
</p>

<p>
Dove possibile, gli utenti sono incoraggiati ad aggiornare le loro macchine
a Debian 12, alias <q>Bookworm</q>, l'attuale rilascio stabile di Debian. Per
far sì che il ciclo dei rilasci Debian sia facile da ricordare, i team coinvolti
hanno stabilito queste periodicità: tre anni di supporto normale, più due anni di
supporto a lungo termine. Debian 12 riceverà il supporto normale sino al 10 giugno
2026 e quello a lungo termine fino al 30 giugno 2028, rispettivamente fino a
tre e cinque anni dalla data del rilascio.
</p>

<p>
Gli utenti che necessitano di rimanere su Debian 11 possono trovare le relative
informazioni a proposito del supporto a lungo termine alla pagina
<a href="https://wiki.debian.org/it/LTS/Using">LTS/Using</a>.
Informazioni importanti e cambiamenti riguardo a Bullseye LTS possono essere
trovate alla pagina
<a href="https://wiki.debian.org/it/LTS/Bullseye">LTS/Bullseye</a>.
</p>

<p>
Gli utenti di Debian 11 LTS sono invitati ad abbonarsi alla
<a href="https://lists.debian.org/debian-lts-announce/">lista dei messaggi degli annunci</a>
per ricevere notifiche sugli aggiornamenti della sicurezza, o a seguire
gli ultimi annunci tramite la pagina web
<a href="https://www.debian.org/lts/security/">informazioni di sicurezza LTS</a>.
</p>

<p>
Alcuni pacchetti non sono coperti dal supporto Bdi Bullseye LTS. I pacchetti
installati e non supportati possono essere identificati installando il
pacchetto
<a href="https://tracker.debian.org/pkg/debian-security-support">debian-security-support</a>.
Se debian-security-support rileva un pacchetto non supportato che si
ritiene importante, contattare
<strong>debian-lts@lists.debian.org</strong>.
</p>

<p>
Debian e il team LTS vogliono ringraziare tutti gli utenti che contribuiscono,
gli sviluppatori, gli sponsor e altre team Debian che hanno reso possibile
l'estensione della vita dei precedenti rilasci stabili, e che hanno fatto
sì che Buster LTS fosse un successo.
</p>

<p>
Se si fa affidamento a Debian LTS, considerare di
<a href="https://wiki.debian.org/LTS/Development">unirsi al team</a>,
fornire patch, farne i collaudi o
<a href="https://wiki.debian.org/LTS/Funding">finanziare le attività</a>.
</p>


<h2>Su Debian</h2>

##  Usually we use that version ...
<p>
Il Progetto Debian è stato fondato nel 1993 da Ian Murdock con il fine di essere un
progetto di una comunità veramente libera. Da lì, il progetto è cresciuto
fino a diventare uno dei più vasti e influenti progetti open source.
Migliaia di volontari da tutto il mondo collaborano al fine di creare e di
mantenere il software Debian. Disponibile in 70 lingue e con una vasta gamma
di architetture supportate, Debian si definisce il
<q>sistema operativo universale</q>.
</p>

<h2>Come contattarci</h2>

<p>Per maggiori informazioni visitare le pagine web su
<a href="$(HOME)/">https://www.debian.org/</a> o mandare un email a
&lt;press@debian.org&gt;.</p>

